package io.ultreia.java4all.decoration;

/*-
 * #%L
 * Decorator API
 * %%
 * Copyright (C) 2021 - 2023 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.List;
import java.util.Locale;
import java.util.Objects;

/**
 * Created on 18/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 0.0.1
 */
public class Decorator {
    /**
     * Decorator definition.
     */
    private final DecoratorDefinition<?, ?> definition;
    /**
     * Decorator configuration.
     */
    private final DecoratorConfiguration configuration;

    public Decorator(DecoratorDefinition<?, ?> definition, Locale locale) {
        this.definition = Objects.requireNonNull(definition);
        this.configuration = new DecoratorConfiguration(definition.properties(), Objects.requireNonNull(locale));
    }

    public Decorator(DecoratorDefinition<?, ?> definition, DecoratorConfiguration configuration) {
        this.definition = Objects.requireNonNull(definition);
        this.configuration = Objects.requireNonNull(configuration);
    }

    public final String decorate(Object source) {
        return configuration.decorate(definition, source);
    }

    public final String decorate(Object source, String rendererSeparator) {
        return configuration.decorate(definition, source, rendererSeparator);
    }

    public final String decorateWithContextSeparator(Object source) {
        return configuration.decorateWithContextSeparator(definition, source);
    }

    public DecoratorDefinition<?, ?> definition() {
        return definition;
    }

    public DecoratorConfiguration configuration() {
        return configuration;
    }

    public final Locale getLocale() {
        return configuration.getLocale();
    }

    public final void setLocale(Locale locale) {
        configuration.setLocale(locale);
    }

    public final int getIndex() {
        return configuration.getIndex();
    }

    public final void setIndex(int index) {
        configuration.setIndex(index);
    }

    public Decorator copy() {
        return new Decorator(definition(), configuration().copy());
    }

    /**
     * Sort a list of data based on the first token property of a given context in a given decorator.
     *
     * @param dataList the list of data to sort
     * @param pos      the index of context to used in decorator to obtain sorted property.
     * @param <O>      type of data
     */
    public <O> void sort(List<O> dataList, int pos) {
        sort(dataList, pos, false);
    }

    /**
     * Sort a list of data based on the first token property of a given context in a given decorator.
     *
     * @param dataList the list of data to sort
     * @param pos      the index of context to used in decorator to obtain sorted property.
     * @param reverse  flag to sort in reverse order if sets to {@code true}
     * @param <O>      type of data
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    public <O> void sort(List<O> dataList, int pos, boolean reverse) {
        configuration.sort(definition, (List) dataList, pos, reverse);
    }
}
