package io.ultreia.java4all.decoration;

/*-
 * #%L
 * Decorator API
 * %%
 * Copyright (C) 2021 - 2023 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.Locale;

/**
 * Created on 18/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 0.0.1
 */
public class DecoratorRenderer<O> implements DecoratedSorter<O> {

    private final Class<O> type;

    public DecoratorRenderer(Class<O> type) {
        this.type = type;
    }

    public Class<O> type() {
        return type;
    }

    public String onNullValue(String propertyName, Locale locale, Object value) {
        return value == null ? null : value.toString();
    }

    public String onNullValue(Locale locale, Object value) {
        return value == null ? null : value.toString();
    }

}
