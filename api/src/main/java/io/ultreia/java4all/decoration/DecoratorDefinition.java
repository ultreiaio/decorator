package io.ultreia.java4all.decoration;

/*-
 * #%L
 * Decorator API
 * %%
 * Copyright (C) 2021 - 2023 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

/**
 * Created on 17/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 0.0.1
 */
public abstract class DecoratorDefinition<O, R extends DecoratorRenderer<O>> {
    public static final String DEFAULT_CONTEXT_SEPARATOR = "##";
    public static final String DEFAULT_RENDERER_SEPARATOR = " - ";
    private final DecoratorKey<O> key;
    private final String contextSeparator;
    private final String rendererSeparator;
    private final R renderer;
    private final int contextCount;
    private final List<String> properties;

    public DecoratorDefinition(Class<O> type, String classifier, String contextSeparator, String rendererSeparator, R renderer, String... properties) {
        this.contextSeparator = Objects.requireNonNull(contextSeparator);
        this.rendererSeparator = Objects.requireNonNull(rendererSeparator);
        this.renderer = Objects.requireNonNull(renderer);
        this.key = new DecoratorKey<>(Objects.requireNonNull(type), classifier);
        this.contextCount = properties.length;
        if (contextCount() == 0) {
            throw new IllegalStateException("Need at least one property");
        }
        this.properties = Arrays.asList(properties);
    }

    public Decorator newDecorator(Locale locale) {
        return new Decorator(this, locale);
    }

    public final String decorate(Locale locale, Object source, int index) {
        return decorate(locale, source, rendererSeparator(), index);
    }

    public abstract String decorate(Locale locale, Object source, String rendererSeparator, int index);

    public abstract String decorateContext(Locale locale, R renderer, O source, int index);

    public int contextCount() {
        return contextCount;
    }

    public Class<O> type() {
        return key.type();
    }

    public String classifier() {
        return key.classifier();
    }

    public String contextSeparator() {
        return contextSeparator;
    }

    public String rendererSeparator() {
        return rendererSeparator;
    }

    public List<String> properties() {
        return properties;
    }

    public DecoratorKey<O> key() {
        return key;
    }

    public DecoratedSorter<O> sorter() {
        return renderer;
    }

    public void sort(Locale locale, int pos, List<O> dataList) {
        ensureContextIndex(pos);
        sorter().sort(this, locale, pos, dataList);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DecoratorDefinition)) return false;
        DecoratorDefinition<?, ?> that = (DecoratorDefinition<?, ?>) o;
        return key.equals(that.key);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key);
    }

    protected void ensureContextIndex(int index) {
        if (index < -1 || index >= contextCount) {
            throw new ArrayIndexOutOfBoundsException(
                    String.format("context index %d is out of bound, can be inside [%d,%d]", index, 0, contextCount - 1));
        }
    }

    protected final String decorateForIndexes(Locale locale, Object source, String rendererSeparator, int... indexes) {
        O cast = type().cast(Objects.requireNonNull(source));
        StringBuilder result = new StringBuilder();
        for (int index : indexes) {
            ensureContextIndex(index);
            String contextContent = decorateContext(locale, renderer, cast, index);
            result.append(rendererSeparator).append(contextContent);
        }
        return result.substring(rendererSeparator.length());
    }

    protected final String decoratorForContextCountEquals1(Locale locale, Object source, String rendererSeparator, int index) {
        ensureContextIndex(index);
        if (index == 0) {
            return decorateForIndexes(locale, source, rendererSeparator, 0);
        }
        throw new IllegalStateException("No index with value: " + index);
    }

    protected final String decoratorForContextCountEquals2(Locale locale, Object source, String rendererSeparator, int index) {
        ensureContextIndex(index);
        switch (index) {
            case 0:
                return decorateForIndexes(locale, source, rendererSeparator, 0, 1);
            case 1:
                return decorateForIndexes(locale, source, rendererSeparator, 1, 0);
            default:
                throw new IllegalStateException("No index with value: " + index);
        }
    }

    protected final String decoratorForContextCountEquals3(Locale locale, Object source, String rendererSeparator, int index) {
        ensureContextIndex(index);
        switch (index) {
            case 0:
                return decorateForIndexes(locale, source, rendererSeparator, 0, 1, 2);
            case 1:
                return decorateForIndexes(locale, source, rendererSeparator, 1, 2, 0);
            case 2:
                return decorateForIndexes(locale, source, rendererSeparator, 2, 0, 1);
            default:
                throw new IllegalStateException("No index with value: " + index);
        }
    }

    protected final String decoratorForContextCountEquals4(Locale locale, Object source, String rendererSeparator, int index) {
        ensureContextIndex(index);
        switch (index) {
            case 0:
                return decorateForIndexes(locale, source, rendererSeparator, 0, 1, 2, 3);
            case 1:
                return decorateForIndexes(locale, source, rendererSeparator, 1, 2, 3, 0);
            case 2:
                return decorateForIndexes(locale, source, rendererSeparator, 2, 3, 0, 1);
            case 3:
                return decorateForIndexes(locale, source, rendererSeparator, 3, 0, 1, 2);
            default:
                throw new IllegalStateException("No index with value: " + index);
        }
    }

    protected final String decoratorForContextCountEquals5(Locale locale, Object source, String rendererSeparator, int index) {
        ensureContextIndex(index);
        switch (index) {
            case 0:
                return decorateForIndexes(locale, source, rendererSeparator, 0, 1, 2, 3, 4);
            case 1:
                return decorateForIndexes(locale, source, rendererSeparator, 1, 2, 3, 4, 0);
            case 2:
                return decorateForIndexes(locale, source, rendererSeparator, 2, 3, 4, 0, 1);
            case 3:
                return decorateForIndexes(locale, source, rendererSeparator, 3, 4, 0, 1, 2);
            case 4:
                return decorateForIndexes(locale, source, rendererSeparator, 4, 0, 1, 2, 3);
            default:
                throw new IllegalStateException("No index with value: " + index);
        }
    }

    protected final String decoratorForContextCountEquals6(Locale locale, Object source, String rendererSeparator, int index) {
        ensureContextIndex(index);
        switch (index) {
            case 0:
                return decorateForIndexes(locale, source, rendererSeparator, 0, 1, 2, 3, 4, 5);
            case 1:
                return decorateForIndexes(locale, source, rendererSeparator, 1, 2, 3, 4, 5, 0);
            case 2:
                return decorateForIndexes(locale, source, rendererSeparator, 2, 3, 4, 5, 0, 1);
            case 3:
                return decorateForIndexes(locale, source, rendererSeparator, 3, 4, 5, 0, 1, 2);
            case 4:
                return decorateForIndexes(locale, source, rendererSeparator, 4, 5, 0, 1, 2, 3);
            case 5:
                return decorateForIndexes(locale, source, rendererSeparator, 5, 0, 1, 2, 3, 4);
            default:
                throw new IllegalStateException("No index with value: " + index);
        }
    }

    protected final String decoratorForContextCountEquals7(Locale locale, Object source, String rendererSeparator, int index) {
        ensureContextIndex(index);
        switch (index) {
            case 0:
                return decorateForIndexes(locale, source, rendererSeparator, 0, 1, 2, 3, 4, 5, 6);
            case 1:
                return decorateForIndexes(locale, source, rendererSeparator, 1, 2, 3, 4, 5, 6, 0);
            case 2:
                return decorateForIndexes(locale, source, rendererSeparator, 2, 3, 4, 5, 6, 0, 1);
            case 3:
                return decorateForIndexes(locale, source, rendererSeparator, 3, 4, 5, 6, 0, 1, 2);
            case 4:
                return decorateForIndexes(locale, source, rendererSeparator, 4, 5, 6, 0, 1, 2, 3);
            case 5:
                return decorateForIndexes(locale, source, rendererSeparator, 5, 6, 0, 1, 2, 3, 4);
            case 6:
                return decorateForIndexes(locale, source, rendererSeparator, 6, 0, 1, 2, 3, 4, 5);
            default:
                throw new IllegalStateException("No index with value: " + index);
        }
    }

    protected final String decoratorForContextCountEquals8(Locale locale, Object source, String rendererSeparator, int index) {
        ensureContextIndex(index);
        switch (index) {
            case 0:
                return decorateForIndexes(locale, source, rendererSeparator, 0, 1, 2, 3, 4, 5, 6, 7);
            case 1:
                return decorateForIndexes(locale, source, rendererSeparator, 1, 2, 3, 4, 5, 6, 7, 0);
            case 2:
                return decorateForIndexes(locale, source, rendererSeparator, 2, 3, 4, 5, 6, 7, 0, 1);
            case 3:
                return decorateForIndexes(locale, source, rendererSeparator, 3, 4, 5, 6, 7, 0, 1, 2);
            case 4:
                return decorateForIndexes(locale, source, rendererSeparator, 4, 5, 6, 7, 0, 1, 2, 3);
            case 5:
                return decorateForIndexes(locale, source, rendererSeparator, 5, 6, 7, 0, 1, 2, 3, 4);
            case 6:
                return decorateForIndexes(locale, source, rendererSeparator, 6, 7, 0, 1, 2, 3, 4, 5);
            case 7:
                return decorateForIndexes(locale, source, rendererSeparator, 7, 0, 1, 2, 3, 4, 5, 6);
            default:
                throw new IllegalStateException("No index with value: " + index);
        }
    }

    protected final String decoratorForContextCountEquals9(Locale locale, Object source, String rendererSeparator, int index) {
        ensureContextIndex(index);
        switch (index) {
            case 0:
                return decorateForIndexes(locale, source, rendererSeparator, 0, 1, 2, 3, 4, 5, 6, 7, 8);
            case 1:
                return decorateForIndexes(locale, source, rendererSeparator, 1, 2, 3, 4, 5, 6, 7, 8, 0);
            case 2:
                return decorateForIndexes(locale, source, rendererSeparator, 2, 3, 4, 5, 6, 7, 8, 0, 1);
            case 3:
                return decorateForIndexes(locale, source, rendererSeparator, 3, 4, 5, 6, 7, 8, 0, 1, 2);
            case 4:
                return decorateForIndexes(locale, source, rendererSeparator, 4, 5, 6, 7, 8, 0, 1, 2, 3);
            case 5:
                return decorateForIndexes(locale, source, rendererSeparator, 5, 6, 7, 8, 0, 1, 2, 3, 4);
            case 6:
                return decorateForIndexes(locale, source, rendererSeparator, 6, 7, 8, 0, 1, 2, 3, 4, 5);
            case 7:
                return decorateForIndexes(locale, source, rendererSeparator, 7, 8, 0, 1, 2, 3, 4, 5, 6);
            case 8:
                return decorateForIndexes(locale, source, rendererSeparator, 8, 0, 1, 2, 3, 4, 5, 6, 7);
            default:
                throw new IllegalStateException("No index with value: " + index);
        }
    }

    protected final String decoratorForContextCountEquals10(Locale locale, Object source, String rendererSeparator, int index) {
        ensureContextIndex(index);
        switch (index) {
            case 0:
                return decorateForIndexes(locale, source, rendererSeparator, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
            case 1:
                return decorateForIndexes(locale, source, rendererSeparator, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0);
            case 2:
                return decorateForIndexes(locale, source, rendererSeparator, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1);
            case 3:
                return decorateForIndexes(locale, source, rendererSeparator, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2);
            case 4:
                return decorateForIndexes(locale, source, rendererSeparator, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3);
            case 5:
                return decorateForIndexes(locale, source, rendererSeparator, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4);
            case 6:
                return decorateForIndexes(locale, source, rendererSeparator, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5);
            case 7:
                return decorateForIndexes(locale, source, rendererSeparator, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6);
            case 8:
                return decorateForIndexes(locale, source, rendererSeparator, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7);
            case 9:
                return decorateForIndexes(locale, source, rendererSeparator, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8);
            default:
                throw new IllegalStateException("No index with value: " + index);
        }
    }

    protected final String decoratorForContextCountEquals11(Locale locale, Object source, String rendererSeparator, int index) {
        ensureContextIndex(index);
        switch (index) {
            case 0:
                return decorateForIndexes(locale, source, rendererSeparator, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
            case 1:
                return decorateForIndexes(locale, source, rendererSeparator, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 0);
            case 2:
                return decorateForIndexes(locale, source, rendererSeparator, 2, 3, 4, 5, 6, 7, 8, 9, 10, 0, 1);
            case 3:
                return decorateForIndexes(locale, source, rendererSeparator, 3, 4, 5, 6, 7, 8, 9, 10, 0, 1, 2);
            case 4:
                return decorateForIndexes(locale, source, rendererSeparator, 4, 5, 6, 7, 8, 9, 10, 0, 1, 2, 3);
            case 5:
                return decorateForIndexes(locale, source, rendererSeparator, 5, 6, 7, 8, 9, 10, 0, 1, 2, 3, 4);
            case 6:
                return decorateForIndexes(locale, source, rendererSeparator, 6, 7, 8, 9, 10, 0, 1, 2, 3, 4, 5);
            case 7:
                return decorateForIndexes(locale, source, rendererSeparator, 7, 8, 9, 10, 0, 1, 2, 3, 4, 5, 6);
            case 8:
                return decorateForIndexes(locale, source, rendererSeparator, 8, 9, 10, 0, 1, 2, 3, 4, 5, 6, 7);
            case 9:
                return decorateForIndexes(locale, source, rendererSeparator, 9, 10, 0, 1, 2, 3, 4, 5, 6, 7, 8);
            case 10:
                return decorateForIndexes(locale, source, rendererSeparator, 10, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
            default:
                throw new IllegalStateException("No index with value: " + index);
        }
    }

    protected final String decoratorForContextCountEquals12(Locale locale, Object source, String rendererSeparator, int index) {
        ensureContextIndex(index);
        switch (index) {
            case 0:
                return decorateForIndexes(locale, source, rendererSeparator, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11);
            case 1:
                return decorateForIndexes(locale, source, rendererSeparator, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 0);
            case 2:
                return decorateForIndexes(locale, source, rendererSeparator, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 0, 1);
            case 3:
                return decorateForIndexes(locale, source, rendererSeparator, 3, 4, 5, 6, 7, 8, 9, 10, 11, 0, 1, 2);
            case 4:
                return decorateForIndexes(locale, source, rendererSeparator, 4, 5, 6, 7, 8, 9, 10, 11, 0, 1, 2, 3);
            case 5:
                return decorateForIndexes(locale, source, rendererSeparator, 5, 6, 7, 8, 9, 10, 11, 0, 1, 2, 3, 4);
            case 6:
                return decorateForIndexes(locale, source, rendererSeparator, 6, 7, 8, 9, 10, 11, 0, 1, 2, 3, 4, 5);
            case 7:
                return decorateForIndexes(locale, source, rendererSeparator, 7, 8, 9, 10, 11, 0, 1, 2, 3, 4, 5, 6);
            case 8:
                return decorateForIndexes(locale, source, rendererSeparator, 8, 9, 10, 11, 0, 1, 2, 3, 4, 5, 6, 7);
            case 9:
                return decorateForIndexes(locale, source, rendererSeparator, 9, 10, 11, 0, 1, 2, 3, 4, 5, 6, 7, 8);
            case 10:
                return decorateForIndexes(locale, source, rendererSeparator, 10, 11, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
            case 11:
                return decorateForIndexes(locale, source, rendererSeparator, 11, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
            default:
                throw new IllegalStateException("No index with value: " + index);
        }
    }

    protected final String decoratorForContextCountEquals13(Locale locale, Object source, String rendererSeparator, int index) {
        ensureContextIndex(index);
        switch (index) {
            case 0:
                return decorateForIndexes(locale, source, rendererSeparator, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12);
            case 1:
                return decorateForIndexes(locale, source, rendererSeparator, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 0);
            case 2:
                return decorateForIndexes(locale, source, rendererSeparator, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 0, 1);
            case 3:
                return decorateForIndexes(locale, source, rendererSeparator, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 0, 1, 2);
            case 4:
                return decorateForIndexes(locale, source, rendererSeparator, 4, 5, 6, 7, 8, 9, 10, 11, 12, 0, 1, 2, 3);
            case 5:
                return decorateForIndexes(locale, source, rendererSeparator, 5, 6, 7, 8, 9, 10, 11, 12, 0, 1, 2, 3, 4);
            case 6:
                return decorateForIndexes(locale, source, rendererSeparator, 6, 7, 8, 9, 10, 11, 12, 0, 1, 2, 3, 4, 5);
            case 7:
                return decorateForIndexes(locale, source, rendererSeparator, 7, 8, 9, 10, 11, 12, 0, 1, 2, 3, 4, 5, 6);
            case 8:
                return decorateForIndexes(locale, source, rendererSeparator, 8, 9, 10, 11, 12, 0, 1, 2, 3, 4, 5, 6, 7);
            case 9:
                return decorateForIndexes(locale, source, rendererSeparator, 9, 10, 11, 12, 0, 1, 2, 3, 4, 5, 6, 7, 8);
            case 10:
                return decorateForIndexes(locale, source, rendererSeparator, 10, 11, 12, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
            case 11:
                return decorateForIndexes(locale, source, rendererSeparator, 11, 12, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
            case 12:
                return decorateForIndexes(locale, source, rendererSeparator, 12, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11);
            default:
                throw new IllegalStateException("No index with value: " + index);
        }
    }

    protected final String decoratorForContextCountEquals14(Locale locale, Object source, String rendererSeparator, int index) {
        ensureContextIndex(index);
        switch (index) {
            case 0:
                return decorateForIndexes(locale, source, rendererSeparator, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13);
            case 1:
                return decorateForIndexes(locale, source, rendererSeparator, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 0);
            case 2:
                return decorateForIndexes(locale, source, rendererSeparator, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 0, 1);
            case 3:
                return decorateForIndexes(locale, source, rendererSeparator, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 0, 1, 2);
            case 4:
                return decorateForIndexes(locale, source, rendererSeparator, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 0, 1, 2, 3);
            case 5:
                return decorateForIndexes(locale, source, rendererSeparator, 5, 6, 7, 8, 9, 10, 11, 12, 13, 0, 1, 2, 3, 4);
            case 6:
                return decorateForIndexes(locale, source, rendererSeparator, 6, 7, 8, 9, 10, 11, 12, 13, 0, 1, 2, 3, 4, 5);
            case 7:
                return decorateForIndexes(locale, source, rendererSeparator, 7, 8, 9, 10, 11, 12, 13, 0, 1, 2, 3, 4, 5, 6);
            case 8:
                return decorateForIndexes(locale, source, rendererSeparator, 8, 9, 10, 11, 12, 13, 0, 1, 2, 3, 4, 5, 6, 7);
            case 9:
                return decorateForIndexes(locale, source, rendererSeparator, 9, 10, 11, 12, 13, 0, 1, 2, 3, 4, 5, 6, 7, 8);
            case 10:
                return decorateForIndexes(locale, source, rendererSeparator, 10, 11, 12, 13, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
            case 11:
                return decorateForIndexes(locale, source, rendererSeparator, 11, 12, 13, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
            case 12:
                return decorateForIndexes(locale, source, rendererSeparator, 12, 13, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11);
            case 13:
                return decorateForIndexes(locale, source, rendererSeparator, 13, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12);
            default:
                throw new IllegalStateException("No index with value: " + index);
        }
    }

    protected final String decoratorForContextCountEquals15(Locale locale, Object source, String rendererSeparator, int index) {
        ensureContextIndex(index);
        switch (index) {
            case 0:
                return decorateForIndexes(locale, source, rendererSeparator, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14);
            case 1:
                return decorateForIndexes(locale, source, rendererSeparator, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 0);
            case 2:
                return decorateForIndexes(locale, source, rendererSeparator, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 0, 1);
            case 3:
                return decorateForIndexes(locale, source, rendererSeparator, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 0, 1, 2);
            case 4:
                return decorateForIndexes(locale, source, rendererSeparator, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 0, 1, 2, 3);
            case 5:
                return decorateForIndexes(locale, source, rendererSeparator, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 0, 1, 2, 3, 4);
            case 6:
                return decorateForIndexes(locale, source, rendererSeparator, 6, 7, 8, 9, 10, 11, 12, 13, 14, 0, 1, 2, 3, 4, 5);
            case 7:
                return decorateForIndexes(locale, source, rendererSeparator, 7, 8, 9, 10, 11, 12, 13, 14, 0, 1, 2, 3, 4, 5, 6);
            case 8:
                return decorateForIndexes(locale, source, rendererSeparator, 8, 9, 10, 11, 12, 13, 14, 0, 1, 2, 3, 4, 5, 6, 7);
            case 9:
                return decorateForIndexes(locale, source, rendererSeparator, 9, 10, 11, 12, 13, 14, 0, 1, 2, 3, 4, 5, 6, 7, 8);
            case 10:
                return decorateForIndexes(locale, source, rendererSeparator, 10, 11, 12, 13, 14, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
            case 11:
                return decorateForIndexes(locale, source, rendererSeparator, 11, 12, 13, 14, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
            case 12:
                return decorateForIndexes(locale, source, rendererSeparator, 12, 13, 14, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11);
            case 13:
                return decorateForIndexes(locale, source, rendererSeparator, 13, 14, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12);
            case 14:
                return decorateForIndexes(locale, source, rendererSeparator, 14, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13);
            default:
                throw new IllegalStateException("No index with value: " + index);
        }
    }

    protected final String decoratorForContextCountEquals16(Locale locale, Object source, String rendererSeparator, int index) {
        ensureContextIndex(index);
        switch (index) {
            case 0:
                return decorateForIndexes(locale, source, rendererSeparator, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15);
            case 1:
                return decorateForIndexes(locale, source, rendererSeparator, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 0);
            case 2:
                return decorateForIndexes(locale, source, rendererSeparator, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 0, 1);
            case 3:
                return decorateForIndexes(locale, source, rendererSeparator, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 0, 1, 2);
            case 4:
                return decorateForIndexes(locale, source, rendererSeparator, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 0, 1, 2, 3);
            case 5:
                return decorateForIndexes(locale, source, rendererSeparator, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 0, 1, 2, 3, 4);
            case 6:
                return decorateForIndexes(locale, source, rendererSeparator, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 0, 1, 2, 3, 4, 5);
            case 7:
                return decorateForIndexes(locale, source, rendererSeparator, 7, 8, 9, 10, 11, 12, 13, 14, 15, 0, 1, 2, 3, 4, 5, 6);
            case 8:
                return decorateForIndexes(locale, source, rendererSeparator, 8, 9, 10, 11, 12, 13, 14, 15, 0, 1, 2, 3, 4, 5, 6, 7);
            case 9:
                return decorateForIndexes(locale, source, rendererSeparator, 9, 10, 11, 12, 13, 14, 15, 0, 1, 2, 3, 4, 5, 6, 7, 8);
            case 10:
                return decorateForIndexes(locale, source, rendererSeparator, 10, 11, 12, 13, 14, 15, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
            case 11:
                return decorateForIndexes(locale, source, rendererSeparator, 11, 12, 13, 14, 15, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
            case 12:
                return decorateForIndexes(locale, source, rendererSeparator, 12, 13, 14, 15, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11);
            case 13:
                return decorateForIndexes(locale, source, rendererSeparator, 13, 14, 15, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12);
            case 14:
                return decorateForIndexes(locale, source, rendererSeparator, 14, 15, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13);
            case 15:
                return decorateForIndexes(locale, source, rendererSeparator, 15, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14);
            default:
                throw new IllegalStateException("No index with value: " + index);
        }
    }

    protected final String decoratorForContextCountEquals17(Locale locale, Object source, String rendererSeparator, int index) {
        ensureContextIndex(index);
        switch (index) {
            case 0:
                return decorateForIndexes(locale, source, rendererSeparator, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16);
            case 1:
                return decorateForIndexes(locale, source, rendererSeparator, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 0);
            case 2:
                return decorateForIndexes(locale, source, rendererSeparator, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 0, 1);
            case 3:
                return decorateForIndexes(locale, source, rendererSeparator, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 0, 1, 2);
            case 4:
                return decorateForIndexes(locale, source, rendererSeparator, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 0, 1, 2, 3);
            case 5:
                return decorateForIndexes(locale, source, rendererSeparator, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 0, 1, 2, 3, 4);
            case 6:
                return decorateForIndexes(locale, source, rendererSeparator, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 0, 1, 2, 3, 4, 5);
            case 7:
                return decorateForIndexes(locale, source, rendererSeparator, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 0, 1, 2, 3, 4, 5, 6);
            case 8:
                return decorateForIndexes(locale, source, rendererSeparator, 8, 9, 10, 11, 12, 13, 14, 15, 16, 0, 1, 2, 3, 4, 5, 6, 7);
            case 9:
                return decorateForIndexes(locale, source, rendererSeparator, 9, 10, 11, 12, 13, 14, 15, 16, 0, 1, 2, 3, 4, 5, 6, 7, 8);
            case 10:
                return decorateForIndexes(locale, source, rendererSeparator, 10, 11, 12, 13, 14, 15, 16, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
            case 11:
                return decorateForIndexes(locale, source, rendererSeparator, 11, 12, 13, 14, 15, 16, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
            case 12:
                return decorateForIndexes(locale, source, rendererSeparator, 12, 13, 14, 15, 16, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11);
            case 13:
                return decorateForIndexes(locale, source, rendererSeparator, 13, 14, 15, 16, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12);
            case 14:
                return decorateForIndexes(locale, source, rendererSeparator, 14, 15, 16, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13);
            case 15:
                return decorateForIndexes(locale, source, rendererSeparator, 15, 16, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14);
            case 16:
                return decorateForIndexes(locale, source, rendererSeparator, 16, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15);
            default:
                throw new IllegalStateException("No index with value: " + index);
        }
    }

    protected final String decoratorForContextCountEquals18(Locale locale, Object source, String rendererSeparator, int index) {
        ensureContextIndex(index);
        switch (index) {
            case 0:
                return decorateForIndexes(locale, source, rendererSeparator, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17);
            case 1:
                return decorateForIndexes(locale, source, rendererSeparator, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 0);
            case 2:
                return decorateForIndexes(locale, source, rendererSeparator, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 0, 1);
            case 3:
                return decorateForIndexes(locale, source, rendererSeparator, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 0, 1, 2);
            case 4:
                return decorateForIndexes(locale, source, rendererSeparator, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 0, 1, 2, 3);
            case 5:
                return decorateForIndexes(locale, source, rendererSeparator, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 0, 1, 2, 3, 4);
            case 6:
                return decorateForIndexes(locale, source, rendererSeparator, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 0, 1, 2, 3, 4, 5);
            case 7:
                return decorateForIndexes(locale, source, rendererSeparator, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 0, 1, 2, 3, 4, 5, 6);
            case 8:
                return decorateForIndexes(locale, source, rendererSeparator, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 0, 1, 2, 3, 4, 5, 6, 7);
            case 9:
                return decorateForIndexes(locale, source, rendererSeparator, 9, 10, 11, 12, 13, 14, 15, 16, 17, 0, 1, 2, 3, 4, 5, 6, 7, 8);
            case 10:
                return decorateForIndexes(locale, source, rendererSeparator, 10, 11, 12, 13, 14, 15, 16, 17, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
            case 11:
                return decorateForIndexes(locale, source, rendererSeparator, 11, 12, 13, 14, 15, 16, 17, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
            case 12:
                return decorateForIndexes(locale, source, rendererSeparator, 12, 13, 14, 15, 16, 17, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11);
            case 13:
                return decorateForIndexes(locale, source, rendererSeparator, 13, 14, 15, 16, 17, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12);
            case 14:
                return decorateForIndexes(locale, source, rendererSeparator, 14, 15, 16, 17, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13);
            case 15:
                return decorateForIndexes(locale, source, rendererSeparator, 15, 16, 17, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14);
            case 16:
                return decorateForIndexes(locale, source, rendererSeparator, 16, 17, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15);
            case 17:
                return decorateForIndexes(locale, source, rendererSeparator, 17, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16);
            default:
                throw new IllegalStateException("No index with value: " + index);
        }
    }

    protected final String decoratorForContextCountEquals19(Locale locale, Object source, String rendererSeparator, int index) {
        ensureContextIndex(index);
        switch (index) {
            case 0:
                return decorateForIndexes(locale, source, rendererSeparator, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18);
            case 1:
                return decorateForIndexes(locale, source, rendererSeparator, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 0);
            case 2:
                return decorateForIndexes(locale, source, rendererSeparator, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 0, 1);
            case 3:
                return decorateForIndexes(locale, source, rendererSeparator, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 0, 1, 2);
            case 4:
                return decorateForIndexes(locale, source, rendererSeparator, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 0, 1, 2, 3);
            case 5:
                return decorateForIndexes(locale, source, rendererSeparator, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 0, 1, 2, 3, 4);
            case 6:
                return decorateForIndexes(locale, source, rendererSeparator, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 0, 1, 2, 3, 4, 5);
            case 7:
                return decorateForIndexes(locale, source, rendererSeparator, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 0, 1, 2, 3, 4, 5, 6);
            case 8:
                return decorateForIndexes(locale, source, rendererSeparator, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 0, 1, 2, 3, 4, 5, 6, 7);
            case 9:
                return decorateForIndexes(locale, source, rendererSeparator, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 0, 1, 2, 3, 4, 5, 6, 7, 8);
            case 10:
                return decorateForIndexes(locale, source, rendererSeparator, 10, 11, 12, 13, 14, 15, 16, 17, 18, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
            case 11:
                return decorateForIndexes(locale, source, rendererSeparator, 11, 12, 13, 14, 15, 16, 17, 18, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
            case 12:
                return decorateForIndexes(locale, source, rendererSeparator, 12, 13, 14, 15, 16, 17, 18, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11);
            case 13:
                return decorateForIndexes(locale, source, rendererSeparator, 13, 14, 15, 16, 17, 18, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12);
            case 14:
                return decorateForIndexes(locale, source, rendererSeparator, 14, 15, 16, 17, 18, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13);
            case 15:
                return decorateForIndexes(locale, source, rendererSeparator, 15, 16, 17, 18, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14);
            case 16:
                return decorateForIndexes(locale, source, rendererSeparator, 16, 17, 18, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15);
            case 17:
                return decorateForIndexes(locale, source, rendererSeparator, 17, 18, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16);
            case 18:
                return decorateForIndexes(locale, source, rendererSeparator, 18, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17);
            default:
                throw new IllegalStateException("No index with value: " + index);
        }
    }

    protected final String decoratorForContextCountEquals20(Locale locale, Object source, String rendererSeparator, int index) {
        ensureContextIndex(index);
        switch (index) {
            case 0:
                return decorateForIndexes(locale, source, rendererSeparator, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19);
            case 1:
                return decorateForIndexes(locale, source, rendererSeparator, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 0);
            case 2:
                return decorateForIndexes(locale, source, rendererSeparator, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 0, 1);
            case 3:
                return decorateForIndexes(locale, source, rendererSeparator, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 0, 1, 2);
            case 4:
                return decorateForIndexes(locale, source, rendererSeparator, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 0, 1, 2, 3);
            case 5:
                return decorateForIndexes(locale, source, rendererSeparator, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 0, 1, 2, 3, 4);
            case 6:
                return decorateForIndexes(locale, source, rendererSeparator, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 0, 1, 2, 3, 4, 5);
            case 7:
                return decorateForIndexes(locale, source, rendererSeparator, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 0, 1, 2, 3, 4, 5, 6);
            case 8:
                return decorateForIndexes(locale, source, rendererSeparator, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 0, 1, 2, 3, 4, 5, 6, 7);
            case 9:
                return decorateForIndexes(locale, source, rendererSeparator, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 0, 1, 2, 3, 4, 5, 6, 7, 8);
            case 10:
                return decorateForIndexes(locale, source, rendererSeparator, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
            case 11:
                return decorateForIndexes(locale, source, rendererSeparator, 11, 12, 13, 14, 15, 16, 17, 18, 19, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
            case 12:
                return decorateForIndexes(locale, source, rendererSeparator, 12, 13, 14, 15, 16, 17, 18, 19, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11);
            case 13:
                return decorateForIndexes(locale, source, rendererSeparator, 13, 14, 15, 16, 17, 18, 19, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12);
            case 14:
                return decorateForIndexes(locale, source, rendererSeparator, 14, 15, 16, 17, 18, 19, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13);
            case 15:
                return decorateForIndexes(locale, source, rendererSeparator, 15, 16, 17, 18, 19, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14);
            case 16:
                return decorateForIndexes(locale, source, rendererSeparator, 16, 17, 18, 19, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15);
            case 17:
                return decorateForIndexes(locale, source, rendererSeparator, 17, 18, 19, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16);
            case 18:
                return decorateForIndexes(locale, source, rendererSeparator, 18, 19, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17);
            case 19:
                return decorateForIndexes(locale, source, rendererSeparator, 19, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18);
            default:
                throw new IllegalStateException("No index with value: " + index);
        }
    }

    protected final String decoratorForContextCountEquals21(Locale locale, Object source, String rendererSeparator, int index) {
        ensureContextIndex(index);
        switch (index) {
            case 0:
                return decorateForIndexes(locale, source, rendererSeparator, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20);
            case 1:
                return decorateForIndexes(locale, source, rendererSeparator, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 0);
            case 2:
                return decorateForIndexes(locale, source, rendererSeparator, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 0, 1);
            case 3:
                return decorateForIndexes(locale, source, rendererSeparator, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 0, 1, 2);
            case 4:
                return decorateForIndexes(locale, source, rendererSeparator, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 0, 1, 2, 3);
            case 5:
                return decorateForIndexes(locale, source, rendererSeparator, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 0, 1, 2, 3, 4);
            case 6:
                return decorateForIndexes(locale, source, rendererSeparator, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 0, 1, 2, 3, 4, 5);
            case 7:
                return decorateForIndexes(locale, source, rendererSeparator, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 0, 1, 2, 3, 4, 5, 6);
            case 8:
                return decorateForIndexes(locale, source, rendererSeparator, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 0, 1, 2, 3, 4, 5, 6, 7);
            case 9:
                return decorateForIndexes(locale, source, rendererSeparator, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 0, 1, 2, 3, 4, 5, 6, 7, 8);
            case 10:
                return decorateForIndexes(locale, source, rendererSeparator, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
            case 11:
                return decorateForIndexes(locale, source, rendererSeparator, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
            case 12:
                return decorateForIndexes(locale, source, rendererSeparator, 12, 13, 14, 15, 16, 17, 18, 19, 20, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11);
            case 13:
                return decorateForIndexes(locale, source, rendererSeparator, 13, 14, 15, 16, 17, 18, 19, 20, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12);
            case 14:
                return decorateForIndexes(locale, source, rendererSeparator, 14, 15, 16, 17, 18, 19, 20, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13);
            case 15:
                return decorateForIndexes(locale, source, rendererSeparator, 15, 16, 17, 18, 19, 20, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14);
            case 16:
                return decorateForIndexes(locale, source, rendererSeparator, 16, 17, 18, 19, 20, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15);
            case 17:
                return decorateForIndexes(locale, source, rendererSeparator, 17, 18, 19, 20, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16);
            case 18:
                return decorateForIndexes(locale, source, rendererSeparator, 18, 19, 20, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17);
            case 19:
                return decorateForIndexes(locale, source, rendererSeparator, 19, 20, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18);
            case 20:
                return decorateForIndexes(locale, source, rendererSeparator, 20, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19);
            default:
                throw new IllegalStateException("No index with value: " + index);
        }
    }

    protected final String decoratorForContextCountEquals22(Locale locale, Object source, String rendererSeparator, int index) {
        ensureContextIndex(index);
        switch (index) {
            case 0:
                return decorateForIndexes(locale, source, rendererSeparator, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21);
            case 1:
                return decorateForIndexes(locale, source, rendererSeparator, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 0);
            case 2:
                return decorateForIndexes(locale, source, rendererSeparator, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 0, 1);
            case 3:
                return decorateForIndexes(locale, source, rendererSeparator, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 0, 1, 2);
            case 4:
                return decorateForIndexes(locale, source, rendererSeparator, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 0, 1, 2, 3);
            case 5:
                return decorateForIndexes(locale, source, rendererSeparator, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 0, 1, 2, 3, 4);
            case 6:
                return decorateForIndexes(locale, source, rendererSeparator, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 0, 1, 2, 3, 4, 5);
            case 7:
                return decorateForIndexes(locale, source, rendererSeparator, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 0, 1, 2, 3, 4, 5, 6);
            case 8:
                return decorateForIndexes(locale, source, rendererSeparator, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 0, 1, 2, 3, 4, 5, 6, 7);
            case 9:
                return decorateForIndexes(locale, source, rendererSeparator, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 0, 1, 2, 3, 4, 5, 6, 7, 8);
            case 10:
                return decorateForIndexes(locale, source, rendererSeparator, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
            case 11:
                return decorateForIndexes(locale, source, rendererSeparator, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
            case 12:
                return decorateForIndexes(locale, source, rendererSeparator, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11);
            case 13:
                return decorateForIndexes(locale, source, rendererSeparator, 13, 14, 15, 16, 17, 18, 19, 20, 21, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12);
            case 14:
                return decorateForIndexes(locale, source, rendererSeparator, 14, 15, 16, 17, 18, 19, 20, 21, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13);
            case 15:
                return decorateForIndexes(locale, source, rendererSeparator, 15, 16, 17, 18, 19, 20, 21, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14);
            case 16:
                return decorateForIndexes(locale, source, rendererSeparator, 16, 17, 18, 19, 20, 21, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15);
            case 17:
                return decorateForIndexes(locale, source, rendererSeparator, 17, 18, 19, 20, 21, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16);
            case 18:
                return decorateForIndexes(locale, source, rendererSeparator, 18, 19, 20, 21, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17);
            case 19:
                return decorateForIndexes(locale, source, rendererSeparator, 19, 20, 21, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18);
            case 20:
                return decorateForIndexes(locale, source, rendererSeparator, 20, 21, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19);
            case 21:
                return decorateForIndexes(locale, source, rendererSeparator, 21, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20);
            default:
                throw new IllegalStateException("No index with value: " + index);
        }
    }

    protected final String decoratorForContextCountEquals23(Locale locale, Object source, String rendererSeparator, int index) {
        ensureContextIndex(index);
        switch (index) {
            case 0:
                return decorateForIndexes(locale, source, rendererSeparator, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22);
            case 1:
                return decorateForIndexes(locale, source, rendererSeparator, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 0);
            case 2:
                return decorateForIndexes(locale, source, rendererSeparator, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 0, 1);
            case 3:
                return decorateForIndexes(locale, source, rendererSeparator, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 0, 1, 2);
            case 4:
                return decorateForIndexes(locale, source, rendererSeparator, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 0, 1, 2, 3);
            case 5:
                return decorateForIndexes(locale, source, rendererSeparator, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 0, 1, 2, 3, 4);
            case 6:
                return decorateForIndexes(locale, source, rendererSeparator, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 0, 1, 2, 3, 4, 5);
            case 7:
                return decorateForIndexes(locale, source, rendererSeparator, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 0, 1, 2, 3, 4, 5, 6);
            case 8:
                return decorateForIndexes(locale, source, rendererSeparator, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 0, 1, 2, 3, 4, 5, 6, 7);
            case 9:
                return decorateForIndexes(locale, source, rendererSeparator, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 0, 1, 2, 3, 4, 5, 6, 7, 8);
            case 10:
                return decorateForIndexes(locale, source, rendererSeparator, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
            case 11:
                return decorateForIndexes(locale, source, rendererSeparator, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
            case 12:
                return decorateForIndexes(locale, source, rendererSeparator, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11);
            case 13:
                return decorateForIndexes(locale, source, rendererSeparator, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12);
            case 14:
                return decorateForIndexes(locale, source, rendererSeparator, 14, 15, 16, 17, 18, 19, 20, 21, 22, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13);
            case 15:
                return decorateForIndexes(locale, source, rendererSeparator, 15, 16, 17, 18, 19, 20, 21, 22, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14);
            case 16:
                return decorateForIndexes(locale, source, rendererSeparator, 16, 17, 18, 19, 20, 21, 22, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15);
            case 17:
                return decorateForIndexes(locale, source, rendererSeparator, 17, 18, 19, 20, 21, 22, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16);
            case 18:
                return decorateForIndexes(locale, source, rendererSeparator, 18, 19, 20, 21, 22, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17);
            case 19:
                return decorateForIndexes(locale, source, rendererSeparator, 19, 20, 21, 22, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18);
            case 20:
                return decorateForIndexes(locale, source, rendererSeparator, 20, 21, 22, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19);
            case 21:
                return decorateForIndexes(locale, source, rendererSeparator, 21, 22, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20);
            case 22:
                return decorateForIndexes(locale, source, rendererSeparator, 22, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21);
            default:
                throw new IllegalStateException("No index with value: " + index);
        }
    }

    protected final String decoratorForContextCountEquals24(Locale locale, Object source, String rendererSeparator, int index) {
        ensureContextIndex(index);
        switch (index) {
            case 0:
                return decorateForIndexes(locale, source, rendererSeparator, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23);
            case 1:
                return decorateForIndexes(locale, source, rendererSeparator, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 0);
            case 2:
                return decorateForIndexes(locale, source, rendererSeparator, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 0, 1);
            case 3:
                return decorateForIndexes(locale, source, rendererSeparator, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 0, 1, 2);
            case 4:
                return decorateForIndexes(locale, source, rendererSeparator, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 0, 1, 2, 3);
            case 5:
                return decorateForIndexes(locale, source, rendererSeparator, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 0, 1, 2, 3, 4);
            case 6:
                return decorateForIndexes(locale, source, rendererSeparator, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 0, 1, 2, 3, 4, 5);
            case 7:
                return decorateForIndexes(locale, source, rendererSeparator, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 0, 1, 2, 3, 4, 5, 6);
            case 8:
                return decorateForIndexes(locale, source, rendererSeparator, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 0, 1, 2, 3, 4, 5, 6, 7);
            case 9:
                return decorateForIndexes(locale, source, rendererSeparator, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 0, 1, 2, 3, 4, 5, 6, 7, 8);
            case 10:
                return decorateForIndexes(locale, source, rendererSeparator, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
            case 11:
                return decorateForIndexes(locale, source, rendererSeparator, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
            case 12:
                return decorateForIndexes(locale, source, rendererSeparator, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11);
            case 13:
                return decorateForIndexes(locale, source, rendererSeparator, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12);
            case 14:
                return decorateForIndexes(locale, source, rendererSeparator, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13);
            case 15:
                return decorateForIndexes(locale, source, rendererSeparator, 15, 16, 17, 18, 19, 20, 21, 22, 23, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14);
            case 16:
                return decorateForIndexes(locale, source, rendererSeparator, 16, 17, 18, 19, 20, 21, 22, 23, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15);
            case 17:
                return decorateForIndexes(locale, source, rendererSeparator, 17, 18, 19, 20, 21, 22, 23, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16);
            case 18:
                return decorateForIndexes(locale, source, rendererSeparator, 18, 19, 20, 21, 22, 23, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17);
            case 19:
                return decorateForIndexes(locale, source, rendererSeparator, 19, 20, 21, 22, 23, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18);
            case 20:
                return decorateForIndexes(locale, source, rendererSeparator, 20, 21, 22, 23, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19);
            case 21:
                return decorateForIndexes(locale, source, rendererSeparator, 21, 22, 23, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20);
            case 22:
                return decorateForIndexes(locale, source, rendererSeparator, 22, 23, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21);
            case 23:
                return decorateForIndexes(locale, source, rendererSeparator, 23, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22);
            default:
                throw new IllegalStateException("No index with value: " + index);
        }
    }

    protected final String decoratorForContextCountEquals25(Locale locale, Object source, String rendererSeparator, int index) {
        ensureContextIndex(index);
        switch (index) {
            case 0:
                return decorateForIndexes(locale, source, rendererSeparator, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24);
            case 1:
                return decorateForIndexes(locale, source, rendererSeparator, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 0);
            case 2:
                return decorateForIndexes(locale, source, rendererSeparator, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 0, 1);
            case 3:
                return decorateForIndexes(locale, source, rendererSeparator, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 0, 1, 2);
            case 4:
                return decorateForIndexes(locale, source, rendererSeparator, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 0, 1, 2, 3);
            case 5:
                return decorateForIndexes(locale, source, rendererSeparator, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 0, 1, 2, 3, 4);
            case 6:
                return decorateForIndexes(locale, source, rendererSeparator, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 0, 1, 2, 3, 4, 5);
            case 7:
                return decorateForIndexes(locale, source, rendererSeparator, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 0, 1, 2, 3, 4, 5, 6);
            case 8:
                return decorateForIndexes(locale, source, rendererSeparator, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 0, 1, 2, 3, 4, 5, 6, 7);
            case 9:
                return decorateForIndexes(locale, source, rendererSeparator, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 0, 1, 2, 3, 4, 5, 6, 7, 8);
            case 10:
                return decorateForIndexes(locale, source, rendererSeparator, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
            case 11:
                return decorateForIndexes(locale, source, rendererSeparator, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
            case 12:
                return decorateForIndexes(locale, source, rendererSeparator, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11);
            case 13:
                return decorateForIndexes(locale, source, rendererSeparator, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12);
            case 14:
                return decorateForIndexes(locale, source, rendererSeparator, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13);
            case 15:
                return decorateForIndexes(locale, source, rendererSeparator, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14);
            case 16:
                return decorateForIndexes(locale, source, rendererSeparator, 16, 17, 18, 19, 20, 21, 22, 23, 24, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15);
            case 17:
                return decorateForIndexes(locale, source, rendererSeparator, 17, 18, 19, 20, 21, 22, 23, 24, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16);
            case 18:
                return decorateForIndexes(locale, source, rendererSeparator, 18, 19, 20, 21, 22, 23, 24, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17);
            case 19:
                return decorateForIndexes(locale, source, rendererSeparator, 19, 20, 21, 22, 23, 24, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18);
            case 20:
                return decorateForIndexes(locale, source, rendererSeparator, 20, 21, 22, 23, 24, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19);
            case 21:
                return decorateForIndexes(locale, source, rendererSeparator, 21, 22, 23, 24, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20);
            case 22:
                return decorateForIndexes(locale, source, rendererSeparator, 22, 23, 24, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21);
            case 23:
                return decorateForIndexes(locale, source, rendererSeparator, 23, 24, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22);
            case 24:
                return decorateForIndexes(locale, source, rendererSeparator, 24, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23);
            default:
                throw new IllegalStateException("No index with value: " + index);
        }
    }

    protected final String decoratorForContextCountEquals26(Locale locale, Object source, String rendererSeparator, int index) {
        ensureContextIndex(index);
        switch (index) {
            case 0:
                return decorateForIndexes(locale, source, rendererSeparator, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25);
            case 1:
                return decorateForIndexes(locale, source, rendererSeparator, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 0);
            case 2:
                return decorateForIndexes(locale, source, rendererSeparator, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 0, 1);
            case 3:
                return decorateForIndexes(locale, source, rendererSeparator, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 0, 1, 2);
            case 4:
                return decorateForIndexes(locale, source, rendererSeparator, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 0, 1, 2, 3);
            case 5:
                return decorateForIndexes(locale, source, rendererSeparator, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 0, 1, 2, 3, 4);
            case 6:
                return decorateForIndexes(locale, source, rendererSeparator, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 0, 1, 2, 3, 4, 5);
            case 7:
                return decorateForIndexes(locale, source, rendererSeparator, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 0, 1, 2, 3, 4, 5, 6);
            case 8:
                return decorateForIndexes(locale, source, rendererSeparator, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 0, 1, 2, 3, 4, 5, 6, 7);
            case 9:
                return decorateForIndexes(locale, source, rendererSeparator, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 0, 1, 2, 3, 4, 5, 6, 7, 8);
            case 10:
                return decorateForIndexes(locale, source, rendererSeparator, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
            case 11:
                return decorateForIndexes(locale, source, rendererSeparator, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
            case 12:
                return decorateForIndexes(locale, source, rendererSeparator, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11);
            case 13:
                return decorateForIndexes(locale, source, rendererSeparator, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12);
            case 14:
                return decorateForIndexes(locale, source, rendererSeparator, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13);
            case 15:
                return decorateForIndexes(locale, source, rendererSeparator, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14);
            case 16:
                return decorateForIndexes(locale, source, rendererSeparator, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15);
            case 17:
                return decorateForIndexes(locale, source, rendererSeparator, 17, 18, 19, 20, 21, 22, 23, 24, 25, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16);
            case 18:
                return decorateForIndexes(locale, source, rendererSeparator, 18, 19, 20, 21, 22, 23, 24, 25, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17);
            case 19:
                return decorateForIndexes(locale, source, rendererSeparator, 19, 20, 21, 22, 23, 24, 25, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18);
            case 20:
                return decorateForIndexes(locale, source, rendererSeparator, 20, 21, 22, 23, 24, 25, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19);
            case 21:
                return decorateForIndexes(locale, source, rendererSeparator, 21, 22, 23, 24, 25, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20);
            case 22:
                return decorateForIndexes(locale, source, rendererSeparator, 22, 23, 24, 25, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21);
            case 23:
                return decorateForIndexes(locale, source, rendererSeparator, 23, 24, 25, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22);
            case 24:
                return decorateForIndexes(locale, source, rendererSeparator, 24, 25, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23);
            case 25:
                return decorateForIndexes(locale, source, rendererSeparator, 25, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24);
            default:
                throw new IllegalStateException("No index with value: " + index);
        }
    }

    protected final String decoratorForContextCountEquals27(Locale locale, Object source, String rendererSeparator, int index) {
        ensureContextIndex(index);
        switch (index) {
            case 0:
                return decorateForIndexes(locale, source, rendererSeparator, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26);
            case 1:
                return decorateForIndexes(locale, source, rendererSeparator, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 0);
            case 2:
                return decorateForIndexes(locale, source, rendererSeparator, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 0, 1);
            case 3:
                return decorateForIndexes(locale, source, rendererSeparator, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 0, 1, 2);
            case 4:
                return decorateForIndexes(locale, source, rendererSeparator, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 0, 1, 2, 3);
            case 5:
                return decorateForIndexes(locale, source, rendererSeparator, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 0, 1, 2, 3, 4);
            case 6:
                return decorateForIndexes(locale, source, rendererSeparator, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 0, 1, 2, 3, 4, 5);
            case 7:
                return decorateForIndexes(locale, source, rendererSeparator, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 0, 1, 2, 3, 4, 5, 6);
            case 8:
                return decorateForIndexes(locale, source, rendererSeparator, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 0, 1, 2, 3, 4, 5, 6, 7);
            case 9:
                return decorateForIndexes(locale, source, rendererSeparator, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 0, 1, 2, 3, 4, 5, 6, 7, 8);
            case 10:
                return decorateForIndexes(locale, source, rendererSeparator, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
            case 11:
                return decorateForIndexes(locale, source, rendererSeparator, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
            case 12:
                return decorateForIndexes(locale, source, rendererSeparator, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11);
            case 13:
                return decorateForIndexes(locale, source, rendererSeparator, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12);
            case 14:
                return decorateForIndexes(locale, source, rendererSeparator, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13);
            case 15:
                return decorateForIndexes(locale, source, rendererSeparator, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14);
            case 16:
                return decorateForIndexes(locale, source, rendererSeparator, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15);
            case 17:
                return decorateForIndexes(locale, source, rendererSeparator, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16);
            case 18:
                return decorateForIndexes(locale, source, rendererSeparator, 18, 19, 20, 21, 22, 23, 24, 25, 26, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17);
            case 19:
                return decorateForIndexes(locale, source, rendererSeparator, 19, 20, 21, 22, 23, 24, 25, 26, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18);
            case 20:
                return decorateForIndexes(locale, source, rendererSeparator, 20, 21, 22, 23, 24, 25, 26, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19);
            case 21:
                return decorateForIndexes(locale, source, rendererSeparator, 21, 22, 23, 24, 25, 26, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20);
            case 22:
                return decorateForIndexes(locale, source, rendererSeparator, 22, 23, 24, 25, 26, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21);
            case 23:
                return decorateForIndexes(locale, source, rendererSeparator, 23, 24, 25, 26, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22);
            case 24:
                return decorateForIndexes(locale, source, rendererSeparator, 24, 25, 26, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23);
            case 25:
                return decorateForIndexes(locale, source, rendererSeparator, 25, 26, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24);
            case 26:
                return decorateForIndexes(locale, source, rendererSeparator, 26, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25);
            default:
                throw new IllegalStateException("No index with value: " + index);
        }
    }

    protected final String decoratorForContextCountEquals28(Locale locale, Object source, String rendererSeparator, int index) {
        ensureContextIndex(index);
        switch (index) {
            case 0:
                return decorateForIndexes(locale, source, rendererSeparator, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27);
            case 1:
                return decorateForIndexes(locale, source, rendererSeparator, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 0);
            case 2:
                return decorateForIndexes(locale, source, rendererSeparator, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 0, 1);
            case 3:
                return decorateForIndexes(locale, source, rendererSeparator, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 0, 1, 2);
            case 4:
                return decorateForIndexes(locale, source, rendererSeparator, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 0, 1, 2, 3);
            case 5:
                return decorateForIndexes(locale, source, rendererSeparator, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 0, 1, 2, 3, 4);
            case 6:
                return decorateForIndexes(locale, source, rendererSeparator, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 0, 1, 2, 3, 4, 5);
            case 7:
                return decorateForIndexes(locale, source, rendererSeparator, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 0, 1, 2, 3, 4, 5, 6);
            case 8:
                return decorateForIndexes(locale, source, rendererSeparator, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 0, 1, 2, 3, 4, 5, 6, 7);
            case 9:
                return decorateForIndexes(locale, source, rendererSeparator, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 0, 1, 2, 3, 4, 5, 6, 7, 8);
            case 10:
                return decorateForIndexes(locale, source, rendererSeparator, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
            case 11:
                return decorateForIndexes(locale, source, rendererSeparator, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
            case 12:
                return decorateForIndexes(locale, source, rendererSeparator, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11);
            case 13:
                return decorateForIndexes(locale, source, rendererSeparator, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12);
            case 14:
                return decorateForIndexes(locale, source, rendererSeparator, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13);
            case 15:
                return decorateForIndexes(locale, source, rendererSeparator, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14);
            case 16:
                return decorateForIndexes(locale, source, rendererSeparator, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15);
            case 17:
                return decorateForIndexes(locale, source, rendererSeparator, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16);
            case 18:
                return decorateForIndexes(locale, source, rendererSeparator, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17);
            case 19:
                return decorateForIndexes(locale, source, rendererSeparator, 19, 20, 21, 22, 23, 24, 25, 26, 27, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18);
            case 20:
                return decorateForIndexes(locale, source, rendererSeparator, 20, 21, 22, 23, 24, 25, 26, 27, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19);
            case 21:
                return decorateForIndexes(locale, source, rendererSeparator, 21, 22, 23, 24, 25, 26, 27, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20);
            case 22:
                return decorateForIndexes(locale, source, rendererSeparator, 22, 23, 24, 25, 26, 27, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21);
            case 23:
                return decorateForIndexes(locale, source, rendererSeparator, 23, 24, 25, 26, 27, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22);
            case 24:
                return decorateForIndexes(locale, source, rendererSeparator, 24, 25, 26, 27, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23);
            case 25:
                return decorateForIndexes(locale, source, rendererSeparator, 25, 26, 27, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24);
            case 26:
                return decorateForIndexes(locale, source, rendererSeparator, 26, 27, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25);
            case 27:
                return decorateForIndexes(locale, source, rendererSeparator, 27, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26);
            default:
                throw new IllegalStateException("No index with value: " + index);
        }
    }

    protected final String decoratorForContextCountEquals29(Locale locale, Object source, String rendererSeparator, int index) {
        ensureContextIndex(index);
        switch (index) {
            case 0:
                return decorateForIndexes(locale, source, rendererSeparator, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28);
            case 1:
                return decorateForIndexes(locale, source, rendererSeparator, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 0);
            case 2:
                return decorateForIndexes(locale, source, rendererSeparator, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 0, 1);
            case 3:
                return decorateForIndexes(locale, source, rendererSeparator, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 0, 1, 2);
            case 4:
                return decorateForIndexes(locale, source, rendererSeparator, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 0, 1, 2, 3);
            case 5:
                return decorateForIndexes(locale, source, rendererSeparator, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 0, 1, 2, 3, 4);
            case 6:
                return decorateForIndexes(locale, source, rendererSeparator, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 0, 1, 2, 3, 4, 5);
            case 7:
                return decorateForIndexes(locale, source, rendererSeparator, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 0, 1, 2, 3, 4, 5, 6);
            case 8:
                return decorateForIndexes(locale, source, rendererSeparator, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 0, 1, 2, 3, 4, 5, 6, 7);
            case 9:
                return decorateForIndexes(locale, source, rendererSeparator, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 0, 1, 2, 3, 4, 5, 6, 7, 8);
            case 10:
                return decorateForIndexes(locale, source, rendererSeparator, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
            case 11:
                return decorateForIndexes(locale, source, rendererSeparator, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
            case 12:
                return decorateForIndexes(locale, source, rendererSeparator, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11);
            case 13:
                return decorateForIndexes(locale, source, rendererSeparator, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12);
            case 14:
                return decorateForIndexes(locale, source, rendererSeparator, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13);
            case 15:
                return decorateForIndexes(locale, source, rendererSeparator, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14);
            case 16:
                return decorateForIndexes(locale, source, rendererSeparator, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15);
            case 17:
                return decorateForIndexes(locale, source, rendererSeparator, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16);
            case 18:
                return decorateForIndexes(locale, source, rendererSeparator, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17);
            case 19:
                return decorateForIndexes(locale, source, rendererSeparator, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18);
            case 20:
                return decorateForIndexes(locale, source, rendererSeparator, 20, 21, 22, 23, 24, 25, 26, 27, 28, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19);
            case 21:
                return decorateForIndexes(locale, source, rendererSeparator, 21, 22, 23, 24, 25, 26, 27, 28, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20);
            case 22:
                return decorateForIndexes(locale, source, rendererSeparator, 22, 23, 24, 25, 26, 27, 28, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21);
            case 23:
                return decorateForIndexes(locale, source, rendererSeparator, 23, 24, 25, 26, 27, 28, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22);
            case 24:
                return decorateForIndexes(locale, source, rendererSeparator, 24, 25, 26, 27, 28, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23);
            case 25:
                return decorateForIndexes(locale, source, rendererSeparator, 25, 26, 27, 28, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24);
            case 26:
                return decorateForIndexes(locale, source, rendererSeparator, 26, 27, 28, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25);
            case 27:
                return decorateForIndexes(locale, source, rendererSeparator, 27, 28, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26);
            case 28:
                return decorateForIndexes(locale, source, rendererSeparator, 28, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27);
            default:
                throw new IllegalStateException("No index with value: " + index);
        }
    }
}
