package io.ultreia.java4all.decoration;

/*-
 * #%L
 * Decorator API
 * %%
 * Copyright (C) 2021 - 2023 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

/**
 * FIXME Add sort here
 * FIXME improve the design to have a list of properties to display (could exclude some properties, could choose any order instead of precomputed circular order)
 * <p>
 * Describes the configuration of a decorator.
 * <p>
 * Created on 13/03/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 0.0.10
 */
public class DecoratorConfiguration {
    /**
     * All available properties for this decorator (order reflect decorator contexts).
     */
    private final List<String> properties;
    /**
     * How many context are available for this decorator.
     */
    private final int contextCount;
    /**
     * Locale used to decorate.
     */
    private Locale locale;
    /**
     * Index used by definition to perform decoration.
     */
    private int index;

    public DecoratorConfiguration(List<String> properties, Locale locale) {
        this.properties = Objects.requireNonNull(properties);
        this.locale = Objects.requireNonNull(locale);
        this.contextCount = properties.size();
        this.index = 0;
    }

    public List<String> getProperties() {
        return properties;
    }

    public int getContextCount() {
        return contextCount;
    }

    public final Locale getLocale() {
        return locale;
    }

    public final void setLocale(Locale locale) {
        this.locale = Objects.requireNonNull(locale);
    }

    public final int getIndex() {
        return index;
    }

    public final void setIndex(int index) {
        ensureContextIndex(index);
        this.index = index;
    }

    public final String decorateWithContextSeparator(DecoratorDefinition<?, ?> definition, Object source) {
        return definition.decorate(locale, source, definition.contextSeparator(), index);
    }

    public String decorate(DecoratorDefinition<?, ?> definition, Object source, String rendererSeparator) {
        return definition.decorate(locale, source, rendererSeparator, index);
    }

    public final String decorate(DecoratorDefinition<?, ?> definition, Object source) {
        return definition.decorate(locale, source, definition.rendererSeparator(), index);
    }

    /**
     * Sort a list of data based on the first token property of a given context in a given decorator.
     *
     * @param definition decorator definition
     * @param dataList   the list of data to sort
     * @param pos        the index of context to used in decorator to obtain sorted property.
     * @param reverse    flag to sort in reverse order if sets to {@code true}
     * @param <O>        type of data
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    public <O> void sort(DecoratorDefinition<?, ?> definition, List<O> dataList, int pos, boolean reverse) {
        setIndex(pos);
        definition.sort(locale, pos, (List) dataList);
        if (reverse) {
            // reverse order
            Collections.reverse(dataList);
        }
    }

    protected void ensureContextIndex(int pos) {
        if (pos < -1 || pos >= contextCount) {
            throw new ArrayIndexOutOfBoundsException(
                    String.format("context index %d is out of bound, can be inside [%d,%d]", pos, 0, contextCount - 1));
        }
    }

    public DecoratorConfiguration copy() {
        DecoratorConfiguration result = new DecoratorConfiguration(getProperties(), getLocale());
        result.setIndex(getIndex());
        return result;
    }
}
