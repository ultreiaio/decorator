package io.ultreia.java4all.decoration;

/*-
 * #%L
 * Decorator API
 * %%
 * Copyright (C) 2021 - 2023 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

/**
 * Created on 20/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 0.0.2
 */
public interface Decorated {

    static String toString(Decorated source, Function<Decorated, String> fallbackIfNoDecorator) {
        String decorate = source.decorate();
        return decorate == null ? fallbackIfNoDecorator.apply(source) : decorate;
    }

    static void registerDecorator(Decorator decorator, Iterable<?> sources) {
        sources.forEach(s -> ((Decorated) s).registerDecorator(decorator));
    }

    static void registerDecorator(Decorator decorator, Stream<Decorated> sources) {
        sources.forEach(s -> s.registerDecorator(decorator));
    }

    Optional<Decorator> decorator();

    void registerDecorator(Decorator decorator);

    default String decorate() {
        return decorator().map(d -> d.decorate(this)).orElse(null);
    }
}
