package io.ultreia.java4all.decoration;

/*-
 * #%L
 * Decorator API
 * %%
 * Copyright (C) 2021 - 2023 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.Set;

/**
 * To initialize via service loader mechanism the decorator provider.
 * <p>
 * Created on 22/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 0.0.3
 */
public interface DecoratorProviderInitializer {

    /**
     * @return properties with string values but should use a numeric sort.
     * @see DecoratedSorter
     */
    Set<String> codeProperties();

    void init(DecoratorProvider provider);
}
