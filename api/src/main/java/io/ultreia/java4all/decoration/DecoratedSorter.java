package io.ultreia.java4all.decoration;

/*-
 * #%L
 * Decorator API
 * %%
 * Copyright (C) 2021 - 2023 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.Comparator;
import java.util.List;
import java.util.Locale;

/**
 * To perform a sort on a list of {@link Decorated} objects.
 * <p>
 * Created on 19/09/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 0.0.6
 */
public interface DecoratedSorter<O> {

    /**
     * Sort the data list.
     * <p>
     * Default behaviour is just to use {@link Object#toString()} (this will use the decorator inside data to do the math).
     *
     * @param definition decorator definition
     * @param locale     locale used to perform sort (can be used to sort some date or I18n)
     * @param pos        context position on which to sort
     * @param dataList   data list to sort
     */
    default void sort(DecoratorDefinition<O, ?> definition, Locale locale, int pos, List<O> dataList) {
        Comparator<O> c = Comparator.comparing(Object::toString);
        dataList.sort(c);
    }

    /**
     * @param definition decorator definition
     * @param locale     locale used to perform sort (can be used to sort some date or I18n)
     * @param pos        context position on which to compare
     * @return the comparator
     */
    default Comparator<O> getComparator(DecoratorDefinition<O, ?> definition, Locale locale, int pos) {
        return Comparator.comparing(d -> definition.decorate(locale, d, pos));
    }
}
