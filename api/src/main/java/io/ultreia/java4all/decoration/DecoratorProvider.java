package io.ultreia.java4all.decoration;

/*-
 * #%L
 * Decorator API
 * %%
 * Copyright (C) 2021 - 2023 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.Closeable;
import java.util.AbstractMap;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.ServiceLoader;
import java.util.function.Consumer;
import java.util.stream.Stream;

/**
 * Created on 18/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 0.0.1
 */
public final class DecoratorProvider implements Closeable {

    private static final Logger log = LogManager.getLogger(DecoratorProvider.class);

    private static DecoratorProvider INSTANCE;

    private final Map<DecoratorKey<?>, DecoratorDefinition<?, ?>> definitions;

    private final transient LoadingCache<Map.Entry<Locale, DecoratorKey<?>>, Decorator> decorators = CacheBuilder.newBuilder().build(new CacheLoader<>() {
        @SuppressWarnings("NullableProblems")
        @Override
        public Decorator load(Map.Entry<Locale, DecoratorKey<?>> key) {
            DecoratorDefinition<?, ?> definition = definition(key.getValue());
            return definition == null ? null : definition.newDecorator(key.getKey());
        }
    });

    public static DecoratorProvider get() {
        return INSTANCE == null ? INSTANCE = new DecoratorProvider() : INSTANCE;
    }

    public static <O> void registerDecorator(Class<O> type, Locale locale, Decorated source) {
        registerDecorator(type, null, locale, source);
    }

    public static <O> void registerDecorator(Class<O> type, Locale locale, Iterable<? extends Decorated> sources) {
        registerDecorator(type, null, locale, sources);
    }

    public static <O> void registerDecorator(Class<O> type, Locale locale, Stream<? extends Decorated> sources) {
        registerDecorator(type, null, locale, sources);
    }

    public static <O, E extends Decorated> void registerDecoratorThen(Class<O> type, Locale locale, Stream<E> sources, Consumer<E> then) {
        registerDecoratorThen(type, null, locale, sources, then);
    }

    public static <O> void registerDecorator(Class<O> type, String classifier, Locale locale, Decorated source) {
        Decorator decorator = DecoratorProvider.get().decorator(locale, type, classifier);
        source.registerDecorator(decorator);
    }

    public static <O> void registerDecorator(Class<O> type, String classifier, Locale locale, Iterable<? extends Decorated> sources) {
        Decorator decorator = DecoratorProvider.get().decorator(locale, type, classifier);
        for (Decorated source : sources) {
            source.registerDecorator(decorator);
        }
    }

    public static <O> void registerDecorator(Class<O> type, String classifier, Locale locale, Stream<? extends Decorated> sources) {
        Decorator decorator = DecoratorProvider.get().decorator(locale, type, classifier);
        sources.forEach(source -> source.registerDecorator(decorator));
    }

    public static <O, E extends Decorated> void registerDecoratorThen(Class<O> type, String classifier, Locale locale, Stream<E> sources, Consumer<E> then) {
        Decorator decorator = DecoratorProvider.get().decorator(locale, type, classifier);
        sources.forEach(source -> {
            source.registerDecorator(decorator);
            then.accept(source);
        });
    }

    public static <O> void registerDecorator(DecoratorKey<O> key, Locale locale, Decorated source) {
        Decorator decorator = DecoratorProvider.get().decorator(locale, key);
        source.registerDecorator(decorator);
    }

    public static <O> void registerDecorator(DecoratorKey<O> key, Locale locale, Iterable<? extends Decorated> sources) {
        Decorator decorator = DecoratorProvider.get().decorator(locale, key);
        for (Decorated source : sources) {
            source.registerDecorator(decorator);
        }
    }

    public static <O> void registerDecorator(DecoratorKey<O> key, Locale locale, Stream<? extends Decorated> sources) {
        Decorator decorator = DecoratorProvider.get().decorator(locale, key);
        sources.forEach(source -> source.registerDecorator(decorator));
    }

    public DecoratorProvider() {
        this.definitions = new LinkedHashMap<>();
        for (DecoratorDefinition<?, ?> definition : ServiceLoader.load(DecoratorDefinition.class)) {
            addDefinition(definition);
        }
        for (DecoratorProviderInitializer initializer : ServiceLoader.load(DecoratorProviderInitializer.class)) {
            initializer.init(this);
        }
        log.info(String.format("Loaded %d decorator definition(s).", definitions.size()));
    }

    public void addDefinition(DecoratorDefinition<?, ?> definition) {
        DecoratorKey<?> type = definition.key();
        DecoratorDefinition<?, ?> oldValue = definitions.put(type, definition);
        if (oldValue != null) {
            throw new IllegalStateException(String.format("Found two decorator definition for same type: %s", type));
        }
        log.debug("Register definition for: " + type);
    }

    public <O> Optional<Decorator> optionalDecorator(Locale locale, Class<O> type) {
        return optionalDecorator(locale, type, null);
    }

    public <O> Optional<Decorator> optionalDecorator(Locale locale, Class<O> type, String classifier) {
        return optionalDecorator(locale, new DecoratorKey<>(type, classifier));
    }

    public <O> Decorator decorator(Locale locale, Class<O> type) {
        return decorator(locale, type, null);
    }

    public <O> Decorator decorator(Locale locale, Class<O> type, String classifier) {
        return decorator(locale, new DecoratorKey<>(type, classifier));
    }

    public <O> Optional<Decorator> optionalDecorator(Locale locale, DecoratorKey<O> key) {
        Map.Entry<Locale, DecoratorKey<?>> cacheKey = new AbstractMap.SimpleEntry<>(locale, key);
        Decorator result = decorators.getIfPresent(cacheKey);
        if (result == null) {
            DecoratorDefinition<O, ?> definition = definition(key);
            if (definition == null) {
                return Optional.empty();
            }
            log.info(String.format("Register decorator: %s", cacheKey));
            result = definition.newDecorator(locale);
            decorators.put(cacheKey, result);
        }
        return Optional.of(result);
    }

    public <O> Decorator decorator(Locale locale, DecoratorKey<O> key) {
        Map.Entry<Locale, DecoratorKey<?>> cacheKey = new AbstractMap.SimpleEntry<>(locale, key);
        Decorator result = decorators.getIfPresent(cacheKey);
        if (result == null) {
            DecoratorDefinition<O, ?> definition = definition(key);
            if (definition == null) {
                throw new IllegalStateException("No decorator definition for cache key: " + cacheKey);
            }
            log.info(String.format("Register decorator: %s", cacheKey));
            result = definition.newDecorator(locale);
            decorators.put(cacheKey, result);
        }
        return result;
    }

    @Override
    public void close() {
        decorators.invalidateAll();
    }

    public Map<DecoratorKey<?>, DecoratorDefinition<?, ?>> definitions() {
        return definitions;
    }

    public <O> DecoratorDefinition<O, ?> definition(Class<O> type) {
        return definition(type, null);
    }

    public <O> DecoratorDefinition<O, ?> definition(Class<O> type, String classifier) {
        return definition(new DecoratorKey<>(type, classifier));
    }

    @SuppressWarnings("unchecked")
    public <O> DecoratorDefinition<O, ?> definition(DecoratorKey<O> type) {
        return (DecoratorDefinition<O, ?>) definitions().get(type);
    }

    public LoadingCache<Map.Entry<Locale, DecoratorKey<?>>, Decorator> decorators() {
        return decorators;
    }
}
