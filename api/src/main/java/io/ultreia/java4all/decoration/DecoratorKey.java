package io.ultreia.java4all.decoration;

/*-
 * #%L
 * Decorator API
 * %%
 * Copyright (C) 2021 - 2023 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.Objects;
import java.util.StringJoiner;

/**
 * Created on 18/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 0.0.1
 */
public class DecoratorKey<O> {

    private final Class<O> type;
    private final String classifier;

    public DecoratorKey(Class<O> type) {
        this(type, null);
    }

    public DecoratorKey(Class<O> type, String classifier) {
        this.type = Objects.requireNonNull(type);
        this.classifier = classifier;
    }

    public Class<O> type() {
        return type;
    }

    public String classifier() {
        return classifier;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DecoratorKey)) return false;
        DecoratorKey<?> that = (DecoratorKey<?>) o;
        return type.equals(that.type) && Objects.equals(classifier, that.classifier);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, classifier);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", DecoratorKey.class.getSimpleName() + "[", "]")
                .add("type=" + type.getName())
                .add("classifier='" + classifier + "'")
                .toString();
    }
}
