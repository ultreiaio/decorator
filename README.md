# Decorator

[![Maven Central
status](https://img.shields.io/maven-central/v/io.ultreia.java4all.decorator/pom.svg)](https://search.maven.org/#search%7Cga%7C1%7Cg%3A%22io.ultreia.java4all.decorator%22%20AND%20a%3A%22pom%22)
![Build Status](https://gitlab.com/ultreiaio/decorator/badges/develop/pipeline.svg)
[![The GNU Lesser General Public License, Version 3.0](https://img.shields.io/badge/license-LGPL3-grren.svg)](http://www.gnu.org/licenses/lgpl-3.0.txt)

This project offers some new API to decorate object

# Abstract

FIXME TODO

# First usage

FIXME TODO

# Decorator API

FIXME TODO

# More to come

FIXME TODO

# Resources

* [Changelog and downloads](https://gitlab.com/ultreiaio/decorator/blob/develop/CHANGELOG.md)
* [Documentation](https://ultreiaio.gitlab.io/decorator)

# Community

* [Contact](mailto:incoming+ultreiaio/decorator@gitlab.com)
