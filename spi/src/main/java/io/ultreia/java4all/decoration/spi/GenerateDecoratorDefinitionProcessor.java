package io.ultreia.java4all.decoration.spi;

/*-
 * #%L
 * Decorator SPI
 * %%
 * Copyright (C) 2021 - 2023 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedOptions;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import javax.tools.Diagnostic;
import javax.tools.JavaFileObject;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created on 19/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 0.0.1
 */
@SupportedAnnotationTypes({"io.ultreia.java4all.decoration.spi.GenerateDecoratorDefinition", "io.ultreia.java4all.decoration.spi.GenerateDecoratorDefinitions"})
@AutoService(Processor.class)
@SupportedOptions({"debug"})
public class GenerateDecoratorDefinitionProcessor extends AbstractProcessor {

    private final Set<String> done = new TreeSet<>();
    private TypeMirror generateDecoratorDefinitionType;
    private TypeMirror generateDecoratorDefinitionsType;

    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.latest();
    }

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        generateDecoratorDefinitionType = processingEnv.getElementUtils().getTypeElement(GenerateDecoratorDefinition.class.getName()).asType();
        generateDecoratorDefinitionsType = processingEnv.getElementUtils().getTypeElement(GenerateDecoratorDefinitions.class.getName()).asType();
        for (TypeElement annotation : annotations) {
            Set<? extends Element> annotatedElements = roundEnv.getElementsAnnotatedWith(annotation);
            if (generateDecoratorDefinitionType.equals(annotation.asType())) {

                for (Element annotatedElement : annotatedElements) {
                    TypeElement classElement = (TypeElement) annotatedElement;

                    String packageName = processingEnv.getElementUtils().getPackageOf(classElement).toString();

                    List<? extends AnnotationMirror> annotationMirrors = processingEnv.getElementUtils().getAllAnnotationMirrors(classElement);
                    AnnotationMirror annotationMirror = annotationMirrors.stream().filter(a -> a.getAnnotationType().equals(generateDecoratorDefinitionType)).findFirst().orElseThrow();

                    Map<? extends ExecutableElement, ? extends AnnotationValue> map = processingEnv.getElementUtils().getElementValuesWithDefaults(annotationMirror);
                    processOneDecoratorDefinition(packageName, map);
                }
                continue;
            }
            if (generateDecoratorDefinitionsType.equals(annotation.asType())) {
                for (Element annotatedElement : annotatedElements) {
                    TypeElement classElement = (TypeElement) annotatedElement;

                    String packageName = processingEnv.getElementUtils().getPackageOf(classElement).toString();

                    List<? extends AnnotationMirror> annotationMirrors = processingEnv.getElementUtils().getAllAnnotationMirrors(classElement);
                    AnnotationMirror annotationMirror = annotationMirrors.stream().filter(a -> a.getAnnotationType().equals(generateDecoratorDefinitionsType)).findFirst().orElseThrow();
                    AnnotationValue value = annotationMirror.getElementValues().entrySet().iterator().next().getValue();
                    List<?> value1 = (List<?>) value.getValue();
                    for (Object o : value1) {
                        Map<? extends ExecutableElement, ? extends AnnotationValue> map = processingEnv.getElementUtils().getElementValuesWithDefaults((AnnotationMirror) o);
                        processOneDecoratorDefinition(packageName, map);
                    }
                }
            }
        }
        return true;
    }

    private void processOneDecoratorDefinition(String packageName, Map<? extends ExecutableElement, ? extends AnnotationValue> map) {
        DecoratorDefinitionJavaFileBuilder javaFileBuilder = DecoratorDefinitionJavaFileBuilder.create(packageName, map);
        String generatedClassName = javaFileBuilder.getQualifiedClassName();
        if (!done.add(generatedClassName)) {
            // Already done
            logWarning(String.format("Skip already processed class: %s", generatedClassName));
            return;
        }
        logDebug(String.format("Detect decorator definition: %s", generatedClassName));
        try {
            String content = javaFileBuilder.generateContent(true);
            generate(generatedClassName, content);

        } catch (IOException e) {
            throw new RuntimeException(String.format("Can't generate decorator definition file for: %s", generatedClassName), e);
        }
    }

    private void generate(String generatedClassName, String content) throws IOException {
        JavaFileObject builderFile = processingEnv.getFiler().createSourceFile(generatedClassName);
        try (PrintWriter out = new PrintWriter(builderFile.openWriter())) {
            out.print(content);
        }
    }

    private void logDebug(String msg) {
        if (processingEnv.getOptions().containsKey("debug")) {
            processingEnv.getMessager().printMessage(Diagnostic.Kind.NOTE, msg);
        }
    }

    private void logWarning(String msg) {
//        if (processingEnv.getOptions().containsKey("debug")) {
        processingEnv.getMessager().printMessage(Diagnostic.Kind.WARNING, msg);
//        }
    }
}


