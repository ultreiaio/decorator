package io.ultreia.java4all.decoration.spi;

/*-
 * #%L
 * Decorator SPI
 * %%
 * Copyright (C) 2021 - 2023 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import io.ultreia.java4all.decoration.DecoratorDefinition;
import io.ultreia.java4all.util.ImportManager;

import javax.annotation.Generated;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.ExecutableElement;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * To generate the content of a java file decorator definition.
 * <p>
 * Created on 05/02/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 0.0.9
 */
public class DecoratorDefinitionJavaFileBuilder {


    private static final String DEFINITION_JAVA_FILE = "package %1$s;\n" +
            "\n" +
            "%2$s" +
            "\n@SuppressWarnings(\"rawtypes\")" +
            "\n@AutoService(DecoratorDefinition.class)" +
            "\n@Generated(value = \"%3$s\"%4$s)" +
            "\npublic final class %10$s extends DecoratorDefinition<%5$s, %6$s<%5$s>> {\n\n" +
            "    public %10$s() {\n" +
            "        super(%5$s.class, %7$s, \"%11$s\", \"%12$s\", new %6$s<>(%5$s.class), %13$s);\n" +
            "    }\n\n" +
            "    @Override\n" +
            "    public final String decorateContext(Locale locale, %6$s<%5$s> renderer, %5$s source, int index) {\n" +
            "        switch (index) {" +
            "%8$s\n" +
            "            default:\n" +
            "                throw new IllegalStateException(\"No index with value: \" + index);\n" +
            "        }\n" +
            "    }\n\n" +
            "    @Override\n" +
            "    public final String decorate(Locale locale, Object source, String rendererSeparator, int index) {\n" +
            "%9$s\n" +
            "    }\n\n" +
            "}";

    private static final String DECORATE_CONTEXT_JAVA_FILE =
            "\n            case %1$s:\n" +
            "              return %2$s;";
    private static final String DECORATE_JAVA_FILE =
            "\n            case %1$s:\n" +
            "                return decorateForIndexes(locale, source, rendererSeparator, %2$s);";
    private final String pattern;
    private final String renderer;
    private final String contextSeparator;
    private final String rendererSeparator;

    private final ImportManager importManager;
    private final String packageName;
    private final String targetSimpleName;
    private final String classifier;
    private final String generatedClassName;

    public static DecoratorDefinitionJavaFileBuilder create(String packageName, Map<? extends ExecutableElement, ? extends AnnotationValue> map) {
        String renderer = null;
        String target = null;
        String pattern = null;
        String classifier = null;
        Boolean prefix = null;
        String contextSeparator = null;
        String rendererSeparator = null;
        for (Map.Entry<? extends ExecutableElement, ? extends AnnotationValue> entry : map.entrySet()) {
            String key = entry.getKey().getSimpleName().toString();
            switch (key) {
                case "target":
                    target = entry.getValue().getValue().toString();
                    break;
                case "prefix":
                    prefix = Boolean.valueOf(entry.getValue().getValue().toString());
                    break;
                case "classifier":
                    classifier = entry.getValue().getValue().toString();

                    break;
                case "renderer":
                    renderer = entry.getValue().getValue().toString();
                    break;
                case "pattern":
                    pattern = entry.getValue().getValue().toString();
                    break;
                case "contextSeparator":
                    contextSeparator = entry.getValue().getValue().toString();
                    break;
                case "rendererSeparator":
                    rendererSeparator = entry.getValue().getValue().toString();
                    break;
            }
        }
        return create(packageName, target, classifier, prefix, pattern, renderer, contextSeparator, rendererSeparator);
    }

    public static DecoratorDefinitionJavaFileBuilder create(String packageName, String target, String classifier, Boolean prefix, String pattern, String renderer, String contextSeparator, String rendererSeparator) {
        return new DecoratorDefinitionJavaFileBuilder(packageName, target, classifier, prefix, pattern, renderer, contextSeparator, rendererSeparator);
    }

    public static DecoratorDefinitionJavaFileBuilder create(String packageName, String target, String pattern, String renderer) {
        return new DecoratorDefinitionJavaFileBuilder(packageName, target, null, false, pattern, renderer, null, null);
    }

    public DecoratorDefinitionJavaFileBuilder(String packageName, String target, String classifier, Boolean prefix, String pattern, String renderer, String contextSeparator, String rendererSeparator) {
        this.packageName = packageName;
        this.importManager = new ImportManager(packageName);
        importManager.addImport(AutoService.class);
        importManager.addImport(Generated.class);
        importManager.addImport(DecoratorDefinition.class);
        importManager.addImport(Optional.class);
        importManager.addImport("io.ultreia.java4all.i18n.I18n");
        importManager.addImport(Locale.class);
        this.pattern = pattern;
        this.targetSimpleName = importManager.addImport(Objects.requireNonNull(target));
        this.renderer = importManager.addImport(Objects.requireNonNull(renderer));
        this.contextSeparator = contextSeparator == null ? DecoratorDefinition.DEFAULT_CONTEXT_SEPARATOR : contextSeparator;
        this.rendererSeparator = rendererSeparator == null ? DecoratorDefinition.DEFAULT_RENDERER_SEPARATOR : rendererSeparator;
        if (classifier != null && classifier.isBlank()) {
            classifier = null;
        }
        this.classifier = classifier;
        String generatedClassName = "";
        if (Objects.requireNonNull(prefix)) {
            generatedClassName += (classifier == null ? "" : classifier) + targetSimpleName;
        } else {
            generatedClassName += targetSimpleName + (classifier == null ? "" : classifier);
        }
        generatedClassName += "DecoratorDefinition";
        this.generatedClassName = generatedClassName;
    }

    public String getPackageName() {
        return packageName;
    }

    public String getQualifiedClassName() {
        return packageName + "." + generatedClassName;
    }

    public String getGeneratedClassName() {
        return generatedClassName;
    }

    public void generateJavaFile(Path targetRootPath, boolean addDate) {
        Path packagePath = targetRootPath.resolve(getPackageName().replaceAll("\\.", File.separator));
        Path targetFile = packagePath.resolve(getGeneratedClassName() + ".java");
        generate(targetFile, generateContent(addDate));
    }

    public String generateContent(boolean addDate) {
        Map<String, String> definitionMap = new DecoratorDefinitionParser().parse(contextSeparator, pattern);
        StringBuilder createGettersBodyBuilder = new StringBuilder();
        StringBuilder decorateBodyBuilder = new StringBuilder();
        StringBuilder propertiesBuilder = new StringBuilder();

        AtomicInteger index = new AtomicInteger();
        int length = definitionMap.size();
        if (length == 0) {
            throw new IllegalStateException("No context found for decorator definition: " + pattern);
        }
        boolean computedContent = length > 11;
        if (computedContent) {
            decorateBodyBuilder.append("        switch (index) {");
        }
        definitionMap.forEach((key, value) -> {
            int currentIndex = index.getAndIncrement();
            createGettersBodyBuilder.append(String.format(DECORATE_CONTEXT_JAVA_FILE, currentIndex, value));
            String order;
            if (length == 1) {
                order = "0";
            } else {
                String[] orders = new String[length];
                for (int i = 0; i < length; i++) {
                    orders[i] = "" + ((currentIndex + i) % length);
                }
                order = String.join(", ", orders);
            }
            propertiesBuilder.append(String.format(", \"%s\"", key));
            if (computedContent) {
                decorateBodyBuilder.append(String.format(DECORATE_JAVA_FILE, currentIndex, order));
            }
        });
        if (computedContent) {
            decorateBodyBuilder.append("            default:\n" +
                                               "                throw new IllegalStateException(\"No index with value: \" + index);\n" +
                                               "        }\n"
            );
        } else {
            decorateBodyBuilder.append(String.format("        return decoratorForContextCountEquals%d(locale, source, rendererSeparator, index);", length));
        }
        String createGetters = createGettersBodyBuilder.toString();
        String decorate = decorateBodyBuilder.toString();
        String imports = importManager.getImportsSection("\n");
        String className = importManager.addImport(generatedClassName);
        String date = addDate?String.format(", date = \"%s\"", new Date()):"";
        return String.format(DEFINITION_JAVA_FILE,
                             packageName,
                             imports,
                             DecoratorDefinitionJavaFileBuilder.class.getName(),
                             date,
                             targetSimpleName,
                             renderer,
                             classifier == null ? null : "\"" + classifier + "\"",
                             createGetters,
                             decorate,
                             className,
                             contextSeparator,
                             rendererSeparator,
                             propertiesBuilder.substring(2));
    }

    protected void generate(Path targetFile, String content) {
        try {
            if (Files.notExists(targetFile.getParent())) {
                Files.createDirectories(targetFile.getParent());
            }
            Files.writeString(targetFile, content);
        } catch (IOException e) {
            throw new IllegalStateException(String.format("Can't generate to: %s", targetFile), e);
        }

    }
}
