package io.ultreia.java4all.decoration.spi;

/*-
 * #%L
 * Decorator SPI
 * %%
 * Copyright (C) 2021 - 2023 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 28/05/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 0.0.12
 */
public class GenerateDecoratorEquals {

    public static final String METHOD_TEMPLATE = "\n" +
            "    protected final String decoratorForContextCountEquals%1$s(Locale locale, Object source, String rendererSeparator, int index) {\n" +
            "        ensureContextIndex(index);\n" +
            "        switch (index) {\n" +
            "%2$s" +
            "            default:\n" +
            "                throw new IllegalStateException(\"No index with value: \" + index);\n" +
            "        }\n" +
            "    }";
    public static final String CASE_TEMPLATE = "" +
            "            case %1$s:\n" +
            "                return decorateForIndexes(locale, source, rendererSeparator, %2$s);\n";

    public static void main(String[] args) {

        for (int i = 12; i < 30; i++) {

            String content = generate(i);
            System.out.println(content);
        }


    }

    private static String generate(int level) {

        List<String> casesBuilder = new ArrayList<>();

        for (int i = 0; i < level; i++) {

            List<String> permutations = new ArrayList<>(level);
            for (int j = 0; j < level; j++) {
                permutations.add("" + (i + j) % level);
            }
            String content = String.format(CASE_TEMPLATE, i, String.join(", ", permutations));
            casesBuilder.add(content);
        }
        return String.format(METHOD_TEMPLATE, level, String.join("", casesBuilder));
    }


}
