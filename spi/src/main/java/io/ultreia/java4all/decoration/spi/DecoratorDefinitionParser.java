package io.ultreia.java4all.decoration.spi;

/*-
 * #%L
 * Decorator SPI
 * %%
 * Copyright (C) 2021 - 2023 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.lang.Strings;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.AbstractMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * To parse decorator description and result some information used to generate to concrete decorator.
 * <p>
 * Created on 17/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 0.0.1
 */
public class DecoratorDefinitionParser {

    private static final Logger log = LogManager.getLogger(DecoratorDefinitionParser.class);

    public Map<String, String> parse(String contextSeparator, String pattern) {
        String[] parts = pattern.split("\\s*" + contextSeparator + "\\s*");
        Map<String, String> result = new LinkedHashMap<>();
        for (String part : parts) {
            Map.Entry<String, String> generatedPair = parsePart(part);
            String generatedPairValue = generatedPair.getValue();
            String generatedPairKey = generatedPair.getKey();
            log.info(String.format("On %s, part %s → %s", part, generatedPairKey, generatedPairValue));
            result.put(generatedPairKey, generatedPairValue);
        }
        return result;
    }

    private Map.Entry<String, String> parsePart(String part) {
        part = "\" " + part + "\"";
        List<String> variables = parseVariables(part);
        String partName = null;
        if (!variables.isEmpty()) {
            partName = variables.get(0);
            int formatterIndexOf = partName.indexOf("::");
            if (formatterIndexOf > -1) {
                String formatter = partName.substring(formatterIndexOf + 2);
                partName = partName.substring(0, formatterIndexOf);
                if ("this".equals(partName)) {
                    partName = formatter;
                }
            }
            int nestedCallIndexOf = partName.indexOf("/");
            if (nestedCallIndexOf > -1) {
                partName = partName.substring(0, nestedCallIndexOf);
            }
            if (partName.startsWith("_")) {
                partName = partName.substring(1);
            }
            log.debug(String.format("On %s, found %d variable(s).", part, variables.size()));
            Map<String, String> generatedVariables = generateVariables(part, variables);
            for (Map.Entry<String, String> entry : generatedVariables.entrySet()) {
                String k = entry.getKey();
                String v = entry.getValue();
                part = part.replace("${" + k + "}", "\" + " + v + " + \"");
            }
        }
        List<String> i18ns = parseI18n(part);
        if (!i18ns.isEmpty()) {
            log.debug(String.format("On %s, found %d i18n key(s).", part, i18ns.size()));
            for (String i18n : i18ns) {
                part = part.replace("$(" + i18n + ")", "\" + I18n.l(locale, \"" + i18n + "\") + \"");
            }
            if (partName == null) {
                partName = "_" + i18ns.get(0);
            }
        }
        part = part.trim();
        if (part.endsWith(" + \"\"")) {
            part = part.substring(0, part.length() - 4);
        }
        if (part.startsWith("\" \" + ")) {
            part = part.substring(5);
        }
        return new AbstractMap.SimpleEntry<>(partName, part.trim());
    }

    public Map<String, String> generateVariables(String part, List<String> variables) {
        Map<String, String> variablesMap = new LinkedHashMap<>();
        for (String variable : variables) {
            String variableGetter = generateVariableGetter(variable);
            variablesMap.put(variable, variableGetter);
            log.debug(String.format("On %s, variable %s → %s", part, variable, variableGetter));
        }
        return variablesMap;
    }

    private String generateVariableGetter(String variable) {
        int formatterIndexOf = variable.indexOf("::");
        String formatter = null;
        if (formatterIndexOf > -1) {
            formatter = variable.substring(formatterIndexOf + 2);
            variable = variable.substring(0, formatterIndexOf);
        }
        String nestedCall = null;
        int nestedCallIndexOf = variable.indexOf("/");
        if (nestedCallIndexOf > -1) {
            nestedCall = variable.substring(nestedCallIndexOf + 1);
            variable = variable.substring(0, nestedCallIndexOf);
        }
        String result;
        boolean booleanVariable = variable.startsWith("_");
        if ("this".equals(variable)) {
            result = "source";
        } else {
            if (booleanVariable) {
                result = String.format("source.is%s()", Strings.capitalize(variable.substring(1)));
            } else {
                result = String.format("source.get%s()", Strings.capitalize(variable));
            }
        }
        if (nestedCall != null) {
            result = String.format("Optional.ofNullable(%s).map(e -> e.get%s()).orElse(null)", result, Strings.capitalize(nestedCall));
        }
        if (formatter != null) {
            result = String.format("renderer.%s(locale, %s)", formatter, result);
        } else {
            result = boxWithOnNullValue(booleanVariable ? variable.substring(1) : variable, result);
        }
        return result;
    }

    protected String boxWithOnNullValue(String partName, String part) {
        return String.format("renderer.onNullValue(\"%s\", locale, %s)", partName, part.trim());
    }

    protected List<String> parseVariables(String part) {
        List<String> result = new LinkedList<>();
        AtomicInteger startIndex = new AtomicInteger();
        int count = 0;
        while (true) {
            String variable = parseVariable(part, startIndex);
            if (variable != null) {
                count++;
                log.debug(String.format("On %s, found variable %d: %s", part, count, variable));
                result.add(variable);
            } else {
                break;
            }
        }
        return result;
    }

    protected List<String> parseI18n(String part) {
        List<String> result = new LinkedList<>();
        AtomicInteger startIndex = new AtomicInteger();
        int count = 0;
        while (true) {
            String variable = parseI18n(part, startIndex);
            if (variable != null) {
                count++;
                log.debug(String.format("On %s, found i18n %d: %s", part, count, variable));
                result.add(variable);
            } else {
                break;
            }
        }
        return result;
    }

    private String parseVariable(String part, AtomicInteger startIndex) {
        int variableStartIndex = part.indexOf("${", startIndex.intValue());
        if (variableStartIndex > -1) {
            int variableEndIndex = part.indexOf("}", variableStartIndex);
            startIndex.set(variableEndIndex);
            return part.substring(variableStartIndex + 2, variableEndIndex);
        }
        return null;
    }

    private String parseI18n(String part, AtomicInteger startIndex) {
        int variableStartIndex = part.indexOf("$(", startIndex.intValue());
        if (variableStartIndex > -1) {
            int variableEndIndex = part.indexOf(")", variableStartIndex);
            startIndex.set(variableEndIndex);
            return part.substring(variableStartIndex + 2, variableEndIndex);
        }
        return null;
    }
}
