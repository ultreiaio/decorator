package io.ultreia.java4all.decoration;

/*-
 * #%L
 * Decorator SPI
 * %%
 * Copyright (C) 2021 - 2023 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.decoration.spi.GenerateDecoratorDefinition;
import io.ultreia.java4all.decoration.spi.GenerateDecoratorDefinitions;

import java.util.Date;
import java.util.Optional;

/**
 * Created on 19/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 0.0.1
 */
@GenerateDecoratorDefinitions({
        @GenerateDecoratorDefinition(pattern = "${id}##${lastUpdateDate}", target = IdDto.class),
        @GenerateDecoratorDefinition(pattern = "${id}##${lastUpdateDate}", classifier = "WithSuffix", target = IdDto.class),
        @GenerateDecoratorDefinition(pattern = "${class}##${id}##${lastUpdateDate}", classifier = "WithPrefix", prefix = true, target = IdDto.class)
})
public class IdDto implements Decorated {

    private String id;

    private Date lastUpdateDate;

    private transient Decorator decorator;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(Date lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    @Override
    public Optional<Decorator> decorator() {
        return Optional.ofNullable(decorator);
    }

    @Override
    public void registerDecorator(Decorator decorator) {
        this.decorator = decorator;
    }

    @Override
    public String toString() {
        return Decorated.toString(this, e -> super.toString());
    }
}
