package io.ultreia.java4all.decoration;

/*-
 * #%L
 * Decorator SPI
 * %%
 * Copyright (C) 2021 - 2023 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.Dates;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;
import java.util.Locale;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created on 18/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 0.0.1
 */
public class ToolkitDecoratorProviderTest {

    private DecoratorProvider provider;

    @Before
    public void setUp() {
        provider = DecoratorProvider.get();
    }

    @After
    public void tearDown() {
        provider.close();
    }

    @Test
    public void init() {
        assertEquals(5, provider.definitions().size());
    }

    @Test
    public void decorator() {

        assertEquals(0, provider.decorators().size());
        Decorator actual = provider.decorator(Locale.FRANCE, IdDto.class);
        assertNotNull(actual);
        assertEquals(1, provider.decorators().size());
        Decorator actual2 = provider.decorator(Locale.FRANCE, IdDto.class);
        assertEquals(actual, actual2);
        assertEquals(1, provider.decorators().size());
        actual2 = provider.decorator(Locale.UK, IdDto.class);
        assertNotEquals(actual, actual2);
        assertEquals(2, provider.decorators().size());
        actual = provider.decorator(Locale.FRANCE, IdDto.class, "WithSuffix");
        assertNotNull(actual);
        actual = provider.decorator(Locale.FRANCE, IdDto.class, "WithPrefix");
        assertNotNull(actual);
    }

    @Test(expected = IllegalStateException.class)
    public void badDecorator() {
        provider.decorator(Locale.FRANCE, IdDto.class, "Taiste");
    }

    @Test(expected = IllegalStateException.class)
    public void badDecorator2() {
        provider.decorator(Locale.FRANCE, Date.class);
    }

    @Test
    public void testIdDto() {
        DecoratorProvider provider = DecoratorProvider.get();
        Decorator decorator = provider.decorator(Locale.FRANCE, IdDto.class);
        assertNotNull(decorator);
        IdDto dto = new IdDto();
        String actual = decorator.decorate(dto);
        System.out.println(actual);
        dto.setId("FirstId");
        actual = decorator.decorate(dto);
        System.out.println(actual);
        System.out.println(dto);
        DecoratorProvider.registerDecorator(IdDto.class, null, decorator.getLocale(), dto);
        System.out.println(dto);
        dto.setLastUpdateDate(Dates.createDate(18, 7, 2021));
        actual = decorator.decorate(dto);
        System.out.println(actual);
        System.out.println(dto);
        dto.registerDecorator(null);
        System.out.println(dto);
        actual = decorator.decorateWithContextSeparator(dto);
        System.out.println(actual);
    }

    @Test
    public void testTwoDto() {
        DecoratorProvider provider = DecoratorProvider.get();
        Decorator decorator = provider.decorator(Locale.FRANCE, TwoDto.class);
        assertNotNull(decorator);
        TwoDto dto = new TwoDto();
        String actual = decorator.decorate(dto);
        System.out.println(actual);
        dto.setId("TwoId");
        actual = decorator.decorate(dto);
        System.out.println(actual);
        System.out.println(dto);
        DecoratorProvider.registerDecorator(TwoDto.class, null, decorator.getLocale(), dto);
        System.out.println(dto);
        dto.setLastUpdateDate(Dates.createDate(18, 7, 2021));
        actual = decorator.decorate(dto);
        System.out.println(actual);
        System.out.println(dto);
        dto.registerDecorator(null);
        System.out.println(dto);
    }
}
