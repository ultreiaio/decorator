package io.ultreia.java4all.decoration.spi;

/*-
 * #%L
 * Decorator SPI
 * %%
 * Copyright (C) 2021 - 2023 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

/**
 * Created on 17/07/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 0.0.1
 */
public class DecoratorDefinitionParserTest {

    private final DecoratorDefinitionParser decoratorDefinitionParser = new DecoratorDefinitionParser();

    @Test
    public void parse() {

        assertParse("$(observe.data.Data.well) Yo", "I18n.l(locale, \"observe.data.Data.well\") + \" Yo\"");
        assertParse("$(observe.data.Data.well) ${wellLabel}##${sampleType/code::label}",
                    "I18n.l(locale, \"observe.data.Data.well\") + \" \" + renderer.onNullValue(\"wellLabel\", locale, source.getWellLabel())",
                    "renderer.label(locale, Optional.ofNullable(source.getSampleType()).map(e -> e.getCode()).orElse(null))");
        assertParse("${wellLabel}##${sampleType::label}",
                    "renderer.onNullValue(\"wellLabel\", locale, source.getWellLabel())",
                    "renderer.label(locale, source.getSampleType())");
        assertParse("${sampleType::label}", "renderer.label(locale, source.getSampleType())");
        assertParse("${_sampleType}", "renderer.onNullValue(\"sampleType\", locale, source.isSampleType())");
        assertParse("${this::label}", "renderer.label(locale, source)");
        assertParse("[${gearTypePrefix}]##$(observe.data.Data.program) ${label}",
                    "\" [\" + renderer.onNullValue(\"gearTypePrefix\", locale, source.getGearTypePrefix()) + \"]\"",
                    "I18n.l(locale, \"observe.data.Data.program\") + \" \" + renderer.onNullValue(\"label\", locale, source.getLabel())");
        assertParse("${settingIdentifier} ( $(settingIdentifier) )##${haulingIdentifier} ( $(haulingIdentifier) )",
                    "renderer.onNullValue(\"settingIdentifier\", locale, source.getSettingIdentifier()) + \" ( \" + I18n.l(locale, \"settingIdentifier\") + \" )\"",
                    "renderer.onNullValue(\"haulingIdentifier\", locale, source.getHaulingIdentifier()) + \" ( \" + I18n.l(locale, \"haulingIdentifier\") + \" )\"");
    }

    @Test
    public void parseVariables() {

        assertParseVariables("$(observe.data.Data.well)");
        assertParseVariables("$(observe.data.Data.well) ${wellLabel}##${sampleType::label}", "wellLabel", "sampleType::label");
        assertParseVariables("${wellLabel}##${sampleType::label}", "wellLabel", "sampleType::label");
        assertParseVariables("${sampleType::label}", "sampleType::label");
    }

    @Test
    public void parseI18n() {

        assertParseI18ns("$(observe.data.Data.well)", "observe.data.Data.well");
        assertParseI18ns("$(observe.data.Data.well) ${wellLabel}##${sampleType::label}", "observe.data.Data.well");
        assertParseI18ns("${wellLabel}##${sampleType::label}");
        assertParseI18ns("${sampleType::label}");
    }

    private void assertParse(String actual, String... expected) {

        Map<String, String> generated = decoratorDefinitionParser.parse("##", actual);
        Assert.assertNotNull(generated);

        int index = 0;
        for (Map.Entry<String, String> entry : generated.entrySet()) {
            String expectedPart = expected[index++];
            String value = entry.getValue();
            assertEquals(expectedPart, value);
        }
    }

    private void assertParseVariables(String actual, String... expected) {
        List<String> parse = decoratorDefinitionParser.parseVariables(actual);
        assertEquals(Arrays.asList(expected), parse);
        Map<String, String> generatedVariables = decoratorDefinitionParser.generateVariables(actual, parse);
        assertEquals(expected.length, generatedVariables.size());
    }

    private void assertParseI18ns(String actual, String... expected) {
        List<String> parse = decoratorDefinitionParser.parseI18n(actual);
        assertEquals(Arrays.asList(expected), parse);
    }
}
