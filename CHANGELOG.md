# Decorator changelog

 * Author [Tony Chemit](mailto:dev@tchemit.fr)
 * Last generated at 2024-09-06 12:10.

## Version [0.0.16](https://gitlab.com/ultreiaio/decorator/-/milestones/16)

**Closed at 2024-09-06.**


### Issues
  * [[enhancement 15]](https://gitlab.com/ultreiaio/decorator/-/issues/15) **Add method DecoratorSorter.getComparator** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [0.0.15](https://gitlab.com/ultreiaio/decorator/-/milestones/15)

**Closed at 2023-06-15.**


### Issues
  * [[bug 14]](https://gitlab.com/ultreiaio/decorator/-/issues/14) **Compiler warns use to add @SuppressWarnings(&quot;rawtypes&quot;) on generated Decorator definitions (google AutoService)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [0.0.14](https://gitlab.com/ultreiaio/decorator/-/milestones/14)

**Closed at 2022-12-23.**


### Issues
  * [[enhancement 13]](https://gitlab.com/ultreiaio/decorator/-/issues/13) **Be able to add (or not!) the date on Generated annotation by DecoratorDefinitionJavaFileBuilder** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [0.0.13](https://gitlab.com/ultreiaio/decorator/-/milestones/13)

**Closed at 2022-10-23.**


### Issues
  * [[enhancement 12]](https://gitlab.com/ultreiaio/decorator/-/issues/12) **Add locale in sort API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [0.0.12](https://gitlab.com/ultreiaio/decorator/-/milestones/12)

**Closed at 2022-05-28.**


### Issues
  * [[enhancement 11]](https://gitlab.com/ultreiaio/decorator/-/issues/11) **Increase number on index count possible (goes to 30)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [0.0.11](https://gitlab.com/ultreiaio/decorator/-/milestones/11)

**Closed at 2022-05-27.**


### Issues
  * [[enhancement 10]](https://gitlab.com/ultreiaio/decorator/-/issues/10) **Replace clone by copy methods** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [0.0.10](https://gitlab.com/ultreiaio/decorator/-/milestones/10)

**Closed at 2022-03-13.**


### Issues
  * [[enhancement 7]](https://gitlab.com/ultreiaio/decorator/-/issues/7) **Add more generated contexts (up to 11 should be ok)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 8]](https://gitlab.com/ultreiaio/decorator/-/issues/8) **Introduce DecoratorConfiguration to store decorator runtime configuration** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 9]](https://gitlab.com/ultreiaio/decorator/-/issues/9) **Can configure which properties which should be sorted as code** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [0.0.8](https://gitlab.com/ultreiaio/decorator/-/milestones/8)

**Closed at 2021-10-25.**


### Issues
  * [[enhancement 4]](https://gitlab.com/ultreiaio/decorator/-/issues/4) **Add optionalDecorator methods in DecoratorProvider** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [0.0.7](https://gitlab.com/ultreiaio/decorator/-/milestones/7)

**Closed at 2021-10-02.**


### Issues
  * [[enhancement 3]](https://gitlab.com/ultreiaio/decorator/-/issues/3) **Add sort position in DecoratedSorted** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [0.0.6](https://gitlab.com/ultreiaio/decorator/-/milestones/6)

**Closed at 2021-09-19.**


### Issues
  * [[enhancement 1]](https://gitlab.com/ultreiaio/decorator/-/issues/1) **Add a way to sort data using the decorator** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 2]](https://gitlab.com/ultreiaio/decorator/-/issues/2) **Make decorator clonable** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [0.0.5](https://gitlab.com/ultreiaio/decorator/-/milestones/5)

**Closed at 2021-07-25.**


### Issues
No issue.

## Version [0.0.4](https://gitlab.com/ultreiaio/decorator/-/milestones/4)

**Closed at 2021-07-23.**


### Issues
No issue.

## Version [0.0.3](https://gitlab.com/ultreiaio/decorator/-/milestones/3)

**Closed at 2021-07-22.**


### Issues
No issue.

## Version [0.0.2](https://gitlab.com/ultreiaio/decorator/-/milestones/2)

**Closed at 2021-07-20.**


### Issues
No issue.

## Version [0.0.1](https://gitlab.com/ultreiaio/decorator/-/milestones/1)

**Closed at 2021-07-19.**


### Issues
No issue.

